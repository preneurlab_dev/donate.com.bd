<?php

require_once 'app/fun/functions.php';

?>

<?php set_page_title('History'); require_once 'header.php'; ?>
<section id="history">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="mission-menu text-center">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#dn-money" role="tab" aria-controls="home" aria-selected="true">Money Donatioon</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#dn-goods" role="tab" aria-controls="profile" aria-selected="false">Awards</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="money-donation-history">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="dn-money" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="all-donate-info">
                                    <h5>Total Donation Amount <span>$545456516</span></h5>
                                    <hr>
                                    <h5>No. of Campaign contribution <span>25</span></h5>
                                    <h5>No. of Donations<span>$545456516</span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="dn-goods" role="tabpanel" aria-labelledby="profile-tab">
                        02
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>Our Mission</h4>
                            </div>
                            <div class="mission-txt">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">

                    </div>

                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <div class="mission-menu">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Money</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Goods</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            01
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Campaign</th>
                                                    <th scope="col">Payment Method</th>
                                                    <th scope="col">Sender No</th>
                                                    <th scope="col">Receiver No</th>
                                                    <th scope="col">Trx ID</th>
                                                    <th scope="col">Statud</th>
                                                    <th scope="col">Isue Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td colspan="2">Larry the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            02
                            <div class="mission-content">
                                <div class="mission-title text-center">
                                    <h4>Our Mission</h4>
                                </div>
                                <div class="mission-txt">

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<section id="search">
    <div class="section-padding">
        <div class="container">

        </div>
    </div>
</section>
<?php require('footer.php'); ?>
