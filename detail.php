<?php

require_once 'app/fun/functions.php';
require_once 'app/autoload.php';

$campaign_info = campaign_info_by_id($_GET['campaign']);

if(empty($campaign_info))
{
    header('location: index.php');
}

?>

    <?php set_page_title('Campaign Details'); require_once 'header.php'; ?>
    <style>
        .welcomte-content {
            width: 100%;
            height: 350px;
        }
    </style>
   <!--    [ Strat Section Title Area]-->
    <section id="welcome" class="">
        <div class="overlay">
            <div class="wel-come-areas owl-carousel">
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Connect. <span>Donate.</span> Impact.</h2>
                                    <h5>Your time to make a difference.</h5>
                                    <div class="welcome-btn">
                                        <a href="registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg-02 d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Verify. <span>Donate.</span> Validate.</h2>
                                    <h5>How many lives you can change today ?</h5>
                                    <div class="welcome-btn">
                                        <a href="registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->

    <!--    [ Strat Section Title Area]-->
    <section id="detail-info">
        <div class="section-padding">
            <div class="container">

                <?php if(isset($_GET['msg']) && $_GET['msg'] == 'updated'): ?>
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                            <strong>Success:</strong> Campaign Updated Successfully.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="donate-money-list">
                        <div class="card">
                            <div class="project-detail-txt">
                                <div class="donate-money-list-title text-center">
                                    <h3><?php echo $campaign_info['campaign_title'] ?></h3>
                                    <p><?php echo $campaign_info['campaign_desc'] ?></p>
                                </div>
                                <div class="project-heading-txt">
                                    <h4>Earned: <?php echo (double)campaign_money_earned(($_GET['campaign'])) ?> TK.</h4>
                                    <h4>Money Donors: <?php echo (int)campaign_money_donors(($_GET['campaign'])) ?></h4>
                                    <h4>Goods Donors: <?php echo (int)campaign_goods_donors(($_GET['campaign'])) ?></h4>
                                </div>
                                <div class="project-progress">
                                    <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (((double)campaign_money_earned($_GET['campaign']))/((double)$campaign_info['target_money']))*100?>%"></div>
                                    <p><?php echo round((((double)campaign_money_earned($_GET['campaign']))/((double)$campaign_info['target_money']))*100, 2)?>%</p>
                                </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="pro-target-money">
                                                <h5>Target : <?php echo $campaign_info['target_money'] ?> TK.</h5>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="pro-target-money text-right">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="contribut">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="conti">
                                                    <div class="pro-target-money">
                                                        <h5>Status: <?php echo $campaign_info['campaign_status'] ?></h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-right">
                                                <div class="conti">
                                                    <div class="pro-target-money">
                                                        <a href="donation/index.php?campaign=<?php echo $_GET['campaign'] ?>">Donate Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row org_info">
                                        <div class="col-lg-12">
                                            <h5>
                                                <img src="<?php echo "site-photos/".organization_info_by_id($campaign_info['org_id'])['org_logo'] ?>" width="50px" height="25px">
                                                <?php echo organization_info_by_id($campaign_info['org_id'])['org_name'] ?>
                                            </h5>
                                                
                                            <h6><?php echo "Address: ".organization_info_by_id($campaign_info['org_id'])['org_address'] ?></h6>
                                            <h6><?php echo "Joined Since: ".date('d M Y', strtotime(organization_info_by_id($campaign_info['org_id'])['issue_date'])) ?></h6>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

<?php require("footer.php"); ?>
