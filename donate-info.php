<?php

require_once 'app/fun/functions.php';

?>

<?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
<section id="login">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">

                    <div class="alert alert-success" role="alert">
                        <h4 class="alert-heading">Warning!</h4>
                        <hr>
                        <h5>Only Donors are applicable to donate.</h5>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>
