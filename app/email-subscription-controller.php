<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/5/18
 * Time: 1:56 PM
 */

require_once 'db/db.php';
require_once 'core/EmailSubscriptions.php';

use \email_subscriptions\EmailSubscriptions as email_subs;

email_subs::db_config($db);

function add_subscription_email($email)
{

    if(email_subs::check_subscription_email_exists($email))
    {
        return 'already_exists';
    }
    else
    {
        return email_subs::add_subscription_email($email);
    }


}

