-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 21, 2018 at 06:54 AM
-- Server version: 5.7.18-1
-- PHP Version: 7.1.6-2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `donation`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `campaign_serial` int(11) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `campaign_title` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `campaign_desc` longtext NOT NULL,
  `org_id` varchar(255) NOT NULL,
  `campaign_photo` text NOT NULL,
  `campaign_start` date NOT NULL,
  `campaign_end` date NOT NULL,
  `campaign_status` varchar(255) NOT NULL,
  `target_money` decimal(60,2) NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`campaign_serial`, `campaign_id`, `campaign_title`, `category`, `campaign_desc`, `org_id`, `campaign_photo`, `campaign_start`, `campaign_end`, `campaign_status`, `target_money`, `issue_date`) VALUES
(1, '11021112575264p13817005no0q5', 'Help Childrens', 'children', 'Help Childrens', '91s3o20p003qn2572217518011o4', '5d97112c1d302541bb35984c.jpg', '2018-07-21', '2018-10-30', 'Ongoing', '47000.00', '2018-07-21');

-- --------------------------------------------------------

--
-- Table structure for table `donor`
--

CREATE TABLE `donor` (
  `donor_serial` int(11) NOT NULL,
  `donor_id` varchar(255) NOT NULL,
  `donor_name` varchar(255) NOT NULL,
  `donor_phone` varchar(255) NOT NULL,
  `donor_photo` text NOT NULL,
  `donor_address` longtext NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donor`
--

INSERT INTO `donor` (`donor_serial`, `donor_id`, `donor_name`, `donor_phone`, `donor_photo`, `donor_address`, `issue_date`) VALUES
(1, '60410258084o1712271o20s5oq21', 'Md.Sayed Ahammed', '01771224089', 'fb822b56117b24551b53c42d.jpg', '519,Road-1, Dhanmondi, Dhaka-1209', '2018-07-21');

-- --------------------------------------------------------

--
-- Table structure for table `goods`
--

CREATE TABLE `goods` (
  `goods_serial` int(11) NOT NULL,
  `donor_id` varchar(255) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `org_id` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `area` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `photo` text NOT NULL,
  `volunteer` varchar(255) NOT NULL,
  `confirm` tinyint(5) NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `money`
--

CREATE TABLE `money` (
  `money_serial` int(11) NOT NULL,
  `donor_id` varchar(255) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `org_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `sender_number` varchar(255) NOT NULL,
  `receiver_number` varchar(255) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `sent_amount` decimal(60,2) NOT NULL,
  `confirm` tinyint(5) NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `org_serial` int(11) NOT NULL,
  `org_id` varchar(255) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `org_logo` text NOT NULL,
  `org_reg_no` varchar(255) NOT NULL,
  `org_reg_details` text NOT NULL,
  `org_address` text NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`org_serial`, `org_id`, `org_name`, `org_logo`, `org_reg_no`, `org_reg_details`, `org_address`, `issue_date`) VALUES
(1, '91s3o20p003qn2572217518011o4', 'We Are 1 Foundation', 'e45129ab32351fb70521d193.jpg', '4569874544', 'This organization is registered by Government.', '25, Mirpur-1, Dhaka, Bangladesh', '2018-07-21');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `serial` int(10) NOT NULL,
  `donor_id` varchar(255) NOT NULL,
  `campaign_id` varchar(255) NOT NULL,
  `org_id` varchar(255) NOT NULL,
  `amount` decimal(60,2) NOT NULL,
  `tx_id` varchar(255) NOT NULL,
  `bank_tx_id` varchar(255) NOT NULL,
  `bank_status` varchar(255) NOT NULL,
  `sp_code` varchar(255) NOT NULL,
  `sp_code_des` varchar(255) NOT NULL,
  `sp_payment_option` varchar(255) NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_serial` int(11) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_serial`, `user_id`, `user_email`, `user_password`, `user_type`) VALUES
(1, '60410258084o1712271o20s5oq21', 'sayed@gmail.com', '$2y$10$DUixESvkXDa1o7Y/SU3nROMQtkE/fVM6mnfRPV8jSxXsj0SkhJkIy', 'donor'),
(2, '91s3o20p003qn2572217518011o4', 'weare1@gmail.com', '$2y$10$WEPZ2bKt3BwqlJfe3lZrdOju9t89mWk0ofLndAzknm/pylwIxOXc2', 'org');

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `volunteer_serial` int(11) NOT NULL,
  `volunteer_id` varchar(255) NOT NULL,
  `volunteer_name` varchar(255) NOT NULL,
  `volunteer_phone` varchar(255) NOT NULL,
  `volunteer_photo` text NOT NULL,
  `volunteer_area` varchar(255) NOT NULL,
  `volunteer_address` text NOT NULL,
  `issue_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`campaign_serial`);

--
-- Indexes for table `donor`
--
ALTER TABLE `donor`
  ADD PRIMARY KEY (`donor_serial`);

--
-- Indexes for table `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`goods_serial`);

--
-- Indexes for table `money`
--
ALTER TABLE `money`
  ADD PRIMARY KEY (`money_serial`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`org_serial`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`serial`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_serial`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`volunteer_serial`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `campaigns`
--
ALTER TABLE `campaigns`
  MODIFY `campaign_serial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `donor`
--
ALTER TABLE `donor`
  MODIFY `donor_serial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `goods`
--
ALTER TABLE `goods`
  MODIFY `goods_serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `money`
--
ALTER TABLE `money`
  MODIFY `money_serial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `org_serial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `serial` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_serial` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `volunteer`
--
ALTER TABLE `volunteer`
  MODIFY `volunteer_serial` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
