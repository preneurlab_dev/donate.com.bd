<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/8/18
 * Time: 11:58 AM
 */

require_once 'core/PaymentAPI.php';
require_once 'core/Campaigns.php';
require_once 'db/db.php';

use \payment_api\PaymentAPI as payment_api;
use \campaigns\Campaigns as campaigns;

payment_api::db_config($db);

function payment_request()
{
    if((!empty($_POST['campaign_id']) || !empty($_POST['org_id'])) && !empty($_POST['amount']))
    {
        $campaign_id = (!empty($_POST['campaign_id'])) ?  $_POST['campaign_id'] : 'None';
        $organization_id = (!empty($_POST['org_id'])) ? $_POST['org_id'] : campaigns::campaign_owner_by_campaign_id($_POST['campaign_id']);
        
        $_SESSION['donor_info'] = array(
            'campaign_id' => $campaign_id,
            'org_id' => $organization_id
        );
    
        
        $amount = (double)$_POST['amount'];
        payment_api::payment_request($amount);
    }
    else
    {
        return 'empty_fields';
    }
    

}

function payment_response()
{
    // Donor Identity
    $campaign_id = $_SESSION['donor_info']['campaign_id'];
    $org_id = $_SESSION['donor_info']['org_id'];
    $donor_id = $_SESSION['auth'];
    unset($_SESSION['donor_info']);

    payment_api::payment_response($donor_id, $campaign_id, $org_id);
}
