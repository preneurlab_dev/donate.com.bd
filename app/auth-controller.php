<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/3/18
 * Time: 1:50 PM
 */

require_once 'db/db.php';
require_once 'core/Auth.php';

use \auth\Auth as auth;

auth::db_config($db);

function user_login()
{
    $email = $_POST['email'];
    $password = $_POST['password'];

    return auth::user_login($email, $password);
}


function user_info()
{
    $user_id = $_SESSION['auth'];
    return auth::user_info($user_id);
}


function verify_old_password($user)
{
    $user_id = null;

    switch ($user)
    {
        case 'donor':
            $user_id = $_POST['donor_id'];
            break;
        case 'volunteer':
            $user_id = $_POST['volunteer_id'];
            break;
        case 'org':
            $user_id = $_POST['org_id'];
            break;
    }


    $old_password = $_POST['old_password'];

    return auth::verify_old_password($user_id,$old_password);
}

function change_password($user)
{
    $user_id = null;

    switch ($user)
    {
        case 'donor':
            $user_id = $_POST['donor_id'];
            break;
        case 'volunteer':
            $user_id = $_POST['volunteer_id'];
            break;
        case 'org':
            $user_id = $_POST['org_id'];
            break;
    }

    $confirm_password = $_POST['confirm_password'];

    return auth::change_password($user_id, $confirm_password);

}