<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:53 PM
 */

require_once 'db/db.php';
require_once 'core/Organization.php';
require_once 'core/Auth.php';

use org\Organization as org;
use \auth\Auth as auth;

org::db_config($db);
auth::db_config($db);



function org_logo_upload($field,$path)
{
    if(is_uploaded_file($_FILES[$field]['tmp_name']))
    {
        if($_FILES[$field]['error'] == UPLOAD_ERR_OK)
        {
            $file_info = new finfo(FILEINFO_MIME_TYPE);
            $fileContents = file_get_contents($_FILES[$field]['tmp_name']);
            $mime_type = $file_info->buffer($fileContents);
            $allowed_type = array('image/gif', 'image/png', 'image/jpeg');

            if(in_array($mime_type, $allowed_type))
            {
                $extension = pathinfo(basename($_FILES[$field]['name']), PATHINFO_EXTENSION);
                $file_name = str_shuffle(uniqid(true).time()).'.'.$extension;

                if(move_uploaded_file($_FILES[$field]['tmp_name'], $path.'/'.$file_name))
                {
                    return $file_name;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return 'invalid_mime';
            }


        }
    }
    else
    {
        return false;
    }
}

function org_reg()
{
    $org_id = str_rot13(str_shuffle(uniqid(true).date('YmdHis')));
    $org_name = safe_string($_POST['org_name']);
    $org_email = valid_email($_POST['org_email']);
    $org_reg_no = valid_numeric($_POST['org_reg_no']);
    $org_pass = password_hash($_POST['org_pass'], PASSWORD_DEFAULT);
    $org_logo = org_logo_upload('org_logo' ,'../site-photos');
    $org_reg_details = safe_string($_POST['org_reg_details']);
    $org_address = safe_string($_POST['org_address']);
    $issue_date = date('Y-m-d');
    
    $org_reg = array(
        $org_id,
        $org_name,
        $org_logo,
        $org_reg_no,
        $org_reg_details,
        $org_address,
        $issue_date
        );
        
    $user_reg = array(
        $org_id,
        $org_email,
        $org_pass,
        'org'
        );

    $not_empty = !(empty($org_name)) && !(empty($org_email)) && !(empty($org_reg_no)) && !(empty($org_pass))
        && !(empty($org_reg_details)) && !(empty($org_address));

    if($org_logo === 'invalid_mime')
    {
        return 'invalid_mime';
    }

    if($not_empty == true)
    {
        if(org::org_reg($org_reg) && auth::user_reg($user_reg))
        {
            return true;
        }
    }
    else
    {
        return 'empty_fields';
    }

}

function org_info_update()
{
    $org_id = $_POST['org_id'];
    $org_name = $_POST['org_name'];
    $org_reg_no = $_POST['org_reg_no'];
    $org_address = $_POST['org_address'];

    $org_info = array(
        $org_name,
        $org_reg_no,
        $org_address,
        $org_id
    );

    return org::org_info_update($org_info);

}


function logo_update()
{
    $org_id = $_POST['org_id'];

    $old_photo = $_POST['old_photo'];

    if (file_exists('../../site-photos/'.$old_photo))
    {
        unlink('../../site-photos/'.$old_photo);
    }

    $org_logo = org_logo_upload('org_logo','../../site-photos');

    $org_id = array(
        $org_logo,
        $org_id
    );

    return org::logo_update($org_id);

}

function organizations()
{
    return org::organizations();
}

function organization_name()
{
    $organization_id = $_SESSION['auth'];
    return org::organization_name($organization_id);
}

function organization_name_by_id($organization_id)
{
    return org::organization_name($organization_id);
}

function organization_info_by_id($organization_id)
{
    return org::organization_info($organization_id);
}

function organization_info()
{
    $organization_id = $_SESSION['auth'];
    return org::organization_info($organization_id);
}
