<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:53 PM
 */

require_once 'db/db.php';
require_once 'core/Donors.php';
require_once 'core/Auth.php';
require_once 'fun/functions.php';

use \donors\Donors as donors;
use \auth\Auth as auth;

donors::db_config($db);
auth::db_config($db);


function donor_photo_upload($field,$path)
{
    if(is_uploaded_file($_FILES[$field]['tmp_name']))
    {
        if($_FILES[$field]['error'] == UPLOAD_ERR_OK)
        {
            $file_info = new finfo(FILEINFO_MIME_TYPE);
            $fileContents = file_get_contents($_FILES[$field]['tmp_name']);
            $mime_type = $file_info->buffer($fileContents);
            $allowed_type = array('image/gif', 'image/png', 'image/jpeg');

            if(in_array($mime_type, $allowed_type))
            {
                $extension = pathinfo(basename($_FILES[$field]['name']), PATHINFO_EXTENSION);
                $file_name = str_shuffle(uniqid(true).time()).'.'.$extension;

                if(move_uploaded_file($_FILES[$field]['tmp_name'], $path.'/'.$file_name))
                {
                    return $file_name;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return 'invalid_mime';
            }


        }
    }
    else
    {
        return false;
    }

}

function donor_reg()
{
    $donor_id = str_rot13(str_shuffle(uniqid(true).date('YmdHis')));
    $donor_name = safe_string($_POST['donor_name']);
    $donor_email = valid_email($_POST['donor_email']);
    $donor_pass = password_hash($_POST['donor_pass'], PASSWORD_DEFAULT);
    $donor_phone = valid_numeric($_POST['donor_phone']);
    $donor_photo = donor_photo_upload('donor_photo','../site-photos');
    $donor_address = safe_string($_POST['donor_address']);
    $issue_date = date('Y-m-d');
    
    $donor_reg = array(
        $donor_id,
        $donor_name,
        $donor_phone,
        $donor_photo,
        $donor_address,
        $issue_date
        );
        
    $user_reg = array(
        $donor_id,
        $donor_email,
        $donor_pass,
        'donor'
        );

    $not_empty = !(empty($donor_name)) && !(empty($donor_email)) && !(empty($donor_pass)) && !(empty($donor_phone))
        && !(empty($donor_photo)) && !(empty($donor_address));

    if($donor_photo === 'invalid_mime')
    {
        return 'invalid_mime';
    }

    if($not_empty == true)
    {
        if(donors::donor_reg($donor_reg) && auth::user_reg($user_reg))
        {
            return true;
        }
    }
    else
    {
        if (file_exists('../site-photos/'.$donor_photo))
        {
            unlink('../site-photos/'.$donor_photo);
        }
        return 'empty_fields';
    }
}

function donor_info()
{
    $donor_id = $_SESSION['auth'];
    return donors::donor_info($donor_id);
}

function donor_name_by_id($donor_id)
{
    return donors::donor_name($donor_id);
}

function donor_name()
{
    $donor_id = $_SESSION['auth'];
    return donors::donor_name($donor_id);
}

function donor_personal_info_update()
{
    $donor_id = $_POST['donor_id'];
    $donor_name = $_POST['donor_name'];
    $donor_phone = $_POST['donor_phone'];
    $donor_address = $_POST['donor_address'];

    $donor_info = array(
        $donor_name,
        $donor_phone,
        $donor_address,
        $donor_id
    );

    return donors::personal_info_update($donor_info);

}

function donor_photo_update()
{
    $donor_id = $_POST['donor_id'];

    $old_photo = $_POST['old_photo'];

    if (file_exists('../../site-photos/'.$old_photo))
    {
        unlink('../../site-photos/'.$old_photo);
    }

    $donor_photo = donor_photo_upload('donor_photo','../../site-photos');

    $donor_info = array(
        $donor_photo,
        $donor_id
    );

    return donors::photo_update($donor_info);

}