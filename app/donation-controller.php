<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 6/4/18
 * Time: 3:53 AM
 */

require_once 'db/db.php';
require_once 'fun/functions.php';
require_once 'core/Donation.php';
require_once 'core/Campaigns.php';

use \donation\Donation as donation;
use \campaigns\Campaigns as campaigns;

donation::db_config($db);
campaigns::db_config($db);


function goods_photo_upload($field,$path)
{
    if(is_uploaded_file($_FILES[$field]['tmp_name']))
    {
        if($_FILES[$field]['error'] == UPLOAD_ERR_OK)
        {
            $extension = pathinfo(basename($_FILES[$field]['name']), PATHINFO_EXTENSION);
            $file_name = str_shuffle(uniqid(true).time()).'.'.$extension;

            if(move_uploaded_file($_FILES[$field]['tmp_name'], $path.'/'.$file_name))
            {
                return $file_name;
            }
            else
            {
                return false;
            }
        }
    }

}

/* Start Money Donations */

date_default_timezone_set('Asia/Dhaka');


/////////////// Start Donor ////////////////////

function donor_total_money_donation()
{
    $donor = $_SESSION['auth'];
    return donation::donor_total_money_donation($donor);
}

function donor_money_donation_contribution()
{
    $donor = $_SESSION['auth'];
    return donation::donor_money_donation_contribution($donor);
}

function donor_money_transaction_logs()
{
    $donor_id = $_SESSION['auth'];
    return donation::donor_money_transaction_logs($donor_id);
}

/////////////// End Donor ////////////////////




//////////////  Start NGO /////////////////////

function ngo_total_money_collected()
{
    $campaigner = $_SESSION['auth'];
    return donation::ngo_total_money_collected($campaigner);
}

function total_money_donors()
{
    $org_id = $_SESSION['auth'];
    return donation::total_money_donors($org_id);
}

function org_money_transaction_logs()
{
    $org_id = $_SESSION['auth'];
    return donation::org_money_transaction_logs($org_id);
}
//////////////  End NGO /////////////////////

/*  End Money Donation */






#   Goods --------------------------------------

/* Start Goods Donations */

function add_goods_donation()
{

    $donor_id = $_SESSION['auth'];
    $campaign_id = (!empty($_POST['campaign_id'])) ?  $_POST['campaign_id'] : 'None';
    $organization_id = (!empty($_POST['org_id'])) ? $_POST['org_id'] : campaign_owner_by_campaign_id($_POST['campaign_id']);

    $category = $_POST['category'];
    $area = $_POST['area'];
    $location = $_POST['location'];
    $photo = goods_photo_upload('goods_photo', '../site-photos');
    $volunteer = 'None';
    $confirm = 0;
    $issue_date = date('Y-m-d');

    $values = array(
        $donor_id,
        $campaign_id,
        $organization_id,
        $category,
        $area,
        $location,
        $photo,
        $volunteer,
        $confirm,
        $issue_date
    );

    return donation::add_goods_donation($values);
}

///////////////////  Start NGO ///////////////////

function donor_goods_donation_submissions()
{
    $org_id = $_SESSION['auth'];
    return donation::donor_goods_donation_submissions($org_id);
}

function volunteer_accepted_goods_list_without_confirm()
{
    return donation::volunteer_accepted_goods_list_without_confirm();
}

function volunteer_goods_donation_confirm_process()
{
    $goods_serial = $_GET['confirm_goods'];
    return donation::volunteer_goods_donation_confirm_process($goods_serial);
}

function volunteer_accepted_goods_list_with_confirm()
{
    return donation::volunteer_accepted_goods_list_with_confirm();
}
///////////////////  End NGO ///////////////////



//////////// Start Volunteer  ///////////////

function total_volunteering()
{
    $volunteer = $_SESSION['auth'];
    return donation::total_volunteering($volunteer);
}

function total_goods_donors()
{
    $campaigner = $_SESSION['auth'];
    return donation::total_goods_donors($campaigner);
}

function volunteer_goods_collection_requests_list()
{
    return donation::volunteer_goods_collection_requests_list();
}

function volunteer_own_accepted_list_without_confirm()
{
    $volunteer = $_SESSION['auth'];
    return donation::volunteer_own_accepted_list_without_confirm($volunteer);
}

function volunteer_goods_collection_accept_process()
{
    $volunteer = $_SESSION['auth'];
    $goods_serial = $_GET['accept'];

    return donation::volunteer_goods_collection_accept_process($volunteer, $goods_serial);
}

function volunteer_own_accepted_list_with_confirm()
{
    $volunteer = $_SESSION['auth'];
    return donation::volunteer_own_accepted_list_with_confirm($volunteer);
}

//////////// End Volunteer  ///////////////



/////////////// Start Donor ////////////////////

function donor_goods_donation_contribution()
{
    $donor = $_SESSION['auth'];
    return donation::donor_goods_donation_contribution($donor);
}

function donor_goods_donations_list_without_confirm()
{
    $donor = $_SESSION['auth'];
    return donation::donor_goods_donations_list_without_confirm($donor);
}

function donor_goods_donations_list_with_confirm()
{
    $donor = $_SESSION['auth'];
    return donation::donor_goods_donations_list_with_confirm($donor);
}

/////////////// End Donor ////////////////////