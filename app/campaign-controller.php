<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 6/4/18
 * Time: 2:57 AM
 */

require_once 'db/db.php';
require_once 'fun/functions.php';
require_once 'core/Campaigns.php';

use \campaigns\Campaigns as campaigns;

campaigns::db_config($db);



function campaign_photo_upload($field,$path)
{
    if(is_uploaded_file($_FILES[$field]['tmp_name']))
    {
        if($_FILES[$field]['error'] == UPLOAD_ERR_OK)
        {
            $allowed_file_extension = array('image/gif', 'image/jpeg', 'image/png');

            if(in_array($_FILES[$field]['type'],$allowed_file_extension))
            {
                $extension = pathinfo(basename($_FILES[$field]['name']), PATHINFO_EXTENSION);
                $file_name = str_shuffle(uniqid(true).time()).'.'.$extension;

                if(move_uploaded_file($_FILES[$field]['tmp_name'], $path.'/'.$file_name))
                {
                    return $file_name;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return 'invalid_file_type';
            }

        }
    }

}



function add_campaign()
{
    $campaign_id = str_rot13(str_shuffle(uniqid(true).date('YmdHis')));
    $campaign_title = safe_string($_POST['campaign_title']);
    $category = safe_string($_POST['category']);
    $campaign_desc = safe_string($_POST['campaign_desc']);
    $org_id = $_SESSION['auth'];
    $campaign_start = date('Y-m-d', strtotime($_POST['campaign_start']));
    $campaign_end = date('Y-m-d', strtotime($_POST['campaign_end']));
    $campaign_status = "Ongoing";
    $campaign_photo = campaign_photo_upload('campaign_photo' ,'../../site-photos');
    $target_amount = (double)$_POST['target_amount'];
    $issue_date = date('Y-m-d');
   
    $values = array(
        $campaign_id,
        $campaign_title,
        $category,
        $campaign_desc,
        $org_id,
        $campaign_photo,
        $campaign_start,
        $campaign_end,
        $campaign_status,
        $target_amount,
        $issue_date
        );

    $not_empty = !(empty($campaign_title)) && !(empty($category)) && !(empty($campaign_desc)) && !(empty($campaign_photo))
                && !(empty($campaign_start)) && !(empty($campaign_end)) && !(empty($target_amount));

    if($campaign_photo === 'invalid_file_type')
    {
        return 'invalid_file_type';
    }
    if ($not_empty === true)
    {
        return campaigns::add_campaign($values);
    }
    else
    {
        return 'empty_fields';
    }

    
}

function campaign_edit()
{
    $campaign_id = safe_string($_POST['campaign_id']);
    $campaign_title = safe_string($_POST['campaign_title']);
    $campaign_desc = safe_string($_POST['campaign_desc']);
    $campaign_start = date('Y-m-d', strtotime($_POST['campaign_start']));
    $campaign_end = date('Y-m-d', strtotime($_POST['campaign_end']));
    $campaign_photo = (!empty($_FILES['campaign_photo']['size'])) ? campaign_photo_upload('campaign_photo' ,'../../site-photos') : $_POST['old_photo'];
    $target_amount = (double)$_POST['target_amount'];

    $values = array(
        $campaign_title,
        $campaign_desc,
        $campaign_photo,
        $campaign_start,
        $campaign_end,
        $target_amount,
        $campaign_id,
    );

    if($campaign_photo === 'invalid_file_type')
    {
        return 'invalid_file_type';
    }
    else
    {
        return campaigns::campaign_edit($values);
    }
}

function campaign_delete()
{
    $campaign_id = $_GET['campaign_id'];
    return campaigns::campaign_delete($campaign_id);
}

function campaign_completed()
{
    $campaign_id = $_GET['campaign'];
    return campaigns::campaign_completed($campaign_id);
}

function campaigns()
{
    return campaigns::campaigns();
}

function index_campaigns()
{
    return campaigns::index_campaigns();
}

function org_campaign_category()
{
    $org_id = $_SESSION['auth'];
    return campaigns::org_campaign_category($org_id);
}

function campaigns_category()
{
    return campaigns::campaigns_category();
}

function org_campaigns_with_category($category)
{
    $org_id = $_SESSION['auth'];
    return campaigns::org_campaigns_with_category($org_id, $category);
}

function campaigns_with_category($category)
{
    return campaigns::campaigns_with_category($category);
}

function org_campaigns_all()
{
    $org_id = $_SESSION['auth'];
    return campaigns::org_campaigns_all($org_id);
}


function ngo_campaigns_count()
{
    $org_id = $_SESSION['auth'];
    return campaigns::ngo_campaigns_count($org_id);
}

function campaign_title_by_id($campaign_id)
{
    return campaigns::campaign_title_by_id($campaign_id);
}

function campaign_info_by_id($campaign_id)
{
    return campaigns::campaign_info_by_id($campaign_id);
}

function campaign_owner_by_campaign_id($campaign_id)
{
    return campaigns::campaign_owner_by_campaign_id($campaign_id);
}

function campaign_money_earned($campaign_id)
{
    return campaigns::campaign_money_earned($campaign_id);
}

function campaign_money_donors($campaign_id)
{
    return campaigns::campaign_money_donors($campaign_id);
}

function campaign_goods_donors($campaign_id)
{
    return campaigns::campaign_goods_donors($campaign_id);
}