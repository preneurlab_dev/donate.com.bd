<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:53 PM
 */

require_once 'db/db.php';
require_once 'core/Volunteers.php';
require_once 'core/Auth.php';

use \volunteers\Volunteers as volunteers;
use \auth\Auth as auth;

volunteers::db_config($db);
auth::db_config($db);


function volunteer_photo_upload($field,$path)
{
    if(is_uploaded_file($_FILES[$field]['tmp_name']))
    {
        if($_FILES[$field]['error'] == UPLOAD_ERR_OK)
        {
            $file_info = new finfo(FILEINFO_MIME_TYPE);
            $fileContents = file_get_contents($_FILES[$field]['tmp_name']);
            $mime_type = $file_info->buffer($fileContents);
            $allowed_type = array('image/gif', 'image/png', 'image/jpeg');

            if(in_array($mime_type, $allowed_type))
            {
                $extension = pathinfo(basename($_FILES[$field]['name']), PATHINFO_EXTENSION);
                $file_name = str_shuffle(uniqid(true).time()).'.'.$extension;

                if(move_uploaded_file($_FILES[$field]['tmp_name'], $path.'/'.$file_name))
                {
                    return $file_name;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return 'invalid_mime';
            }


        }
    }
    else
    {
        return false;
    }

}


function volunteer_reg()
{
    $volunteer_id = str_rot13(str_shuffle(uniqid(true).date('YmdHis')));
    $volunteer_name = safe_string($_POST['volunteer_name']);
    $volunteer_email = valid_email($_POST['volunteer_email']);
    $volunteer_pass = password_hash($_POST['volunteer_pass'], PASSWORD_DEFAULT);
    $volunteer_phone = valid_numeric($_POST['volunteer_phone']);
    $volunteer_area = safe_string($_POST['volunteer_area']);
    $volunteer_photo = volunteer_photo_upload('volunteer_photo','../site-photos');
    $volunteer_address = safe_string($_POST['volunteer_address']);
    $issue_date = date('Y-m-d');
    
    $volunteer_reg = array(
        $volunteer_id,
        $volunteer_name,
        $volunteer_phone,
        $volunteer_photo,
        $volunteer_area,
        $volunteer_address,
        $issue_date
        );
        
    $user_reg = array(
        $volunteer_id,
        $volunteer_email,
        $volunteer_pass,
        'volunteer'
        );

    $not_empty = !(empty($volunteer_name)) && !(empty($volunteer_email)) && !(empty($volunteer_pass)) && !(empty($volunteer_phone))
        && !(empty($volunteer_area)) && !(empty($volunteer_address));

    if($volunteer_photo === 'invalid_mime')
    {
        return 'invalid_mime';
    }

    if($not_empty == true)
    {
        if(volunteers::volunteer_reg($volunteer_reg) && auth::user_reg($user_reg))
        {
            return true;
        }
    }
    else
    {
        if (file_exists('../site-photos/'.$volunteer_photo))
        {
            unlink('../site-photos/'.$volunteer_photo);
        }

        return 'empty_fields';
    }
}

function volunteer_info()
{
    $volunteer_id = $_SESSION['auth'];
    return volunteers::volunteer_info($volunteer_id);
}

function volunteer_name()
{
    $volunteer_id = $_SESSION['auth'];
    return volunteers::volunteer_name($volunteer_id);
}

function volunteer_name_by_id($volunteer_id)
{
    return volunteers::volunteer_name($volunteer_id);
}

function personal_info_update()
{
    $volunteer_id = $_POST['volunteer_id'];
    $volunteer_name = $_POST['volunteer_name'];
    $volunteer_phone = $_POST['volunteer_phone'];
    $volunteer_address = $_POST['volunteer_address'];

    $volunteer_info = array(
        $volunteer_name,
        $volunteer_phone,
        $volunteer_address,
        $volunteer_id
    );

    return volunteers::personal_info_update($volunteer_info);

}

function photo_update()
{
    $volunteer_id = $_POST['volunteer_id'];
    $old_photo = $_POST['old_photo'];

    if (file_exists('../../site-photos/'.$old_photo))
    {
        unlink('../../site-photos/'.$old_photo);
    }

    $volunteer_photo = volunteer_photo_upload('volunteer_photo','../../site-photos');

    $volunteer_info = array(
        $volunteer_photo,
        $volunteer_id
    );

    return volunteers::photo_update($volunteer_info);

}