<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 6/4/18
 * Time: 2:59 AM
 */

$page_title = null;

function set_page_title($title)
{
    global $page_title;
    $page_title = $title;
}

function get_page_title()
{
    global $page_title;
    echo $page_title;
}

function set_alert_message($msg)
{
    global $alert_msg;
    $alert_msg = $msg;
}

function get_alert_message()
{
    global $alert_msg;

    if(!empty($alert_msg))
    {
        echo "<script>$.notify("."'".$alert_msg."'".");</script>";
    }
    else
    {
        return false;
    }


}

function session()
{
    session_name('_user');
    $expire_time = (60*60)*24*7; //  Days
    $secure = isset($_SERVER["HTTPS"]);
    session_set_cookie_params($expire_time,'/','',$secure,1);
    session_start();
}

session();

function logged_in_redirect()
{

    if(isset($_SESSION['auth']))
    {
        if ($_SESSION['type'] === 'donor')
        {
            header('Location: dashboard/donor/index.php');
        }
        elseif ($_SESSION['type'] === 'volunteer')
        {
            header('Location: dashboard/volunteer/index.php');
        }
        elseif ($_SESSION['type'] === 'org')
        {
            header('Location: dashboard/org/index.php');
        }
    }
}

function unauthorized_user_redirect($url)
{
    if (!isset($_SESSION['auth']))
    {
        header('Location:'.$url);
    }
}

function encryption($str)
{
    return openssl_encrypt($str,"AES-128-ECB",'925032547');
}

function decryption($str)
{
    return openssl_decrypt($str,"AES-128-ECB",'925032547');
}

function redirect_page($page)
{
    setcookie('_link', encryption($page), time()+(3600*24), '/', '', '0', '1');
}

function logout()
{
    session_unset();
    setcookie('_link', '', time()-(3600*24), '/', '', '0', '1');

    session_destroy();

    if (!isset($_SESSION['auth']))
    {
        header('Location: index.php');
    }
}

function safe_string($str)
{
    $safe_str = strip_tags($str);
    return htmlspecialchars($safe_str, ENT_QUOTES);
}

function valid_email($email)
{
    if(filter_var($email, FILTER_VALIDATE_EMAIL))
    {
        return filter_var($email, FILTER_SANITIZE_EMAIL);
    }
    else
    {
        return false;
    }
}

function valid_numeric($str)
{
    if(is_numeric($str))
    {
        return $str;
    }
    else
    {
        return false;
    }
}


