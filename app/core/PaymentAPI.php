<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/8/18
 * Time: 11:45 AM
 */

namespace payment_api;


class PaymentAPI {

    private static $db;
    private static $merchant_username = 'preneur';
    private static $merchant_password = 'zhXJB6QS';
    private static $return_url = 'https://www.donate.com.bd/demo/payment-return-url.php';
    private static $success_url = 'https://www.donate.com.bd/demo/payment-status.php';
    private static $failed_url = 'https://www.donate.com.bd/demo/payment-status.php';

    public static function db_config($config)
    {
        self::$db = $config;
    }

    public static function payment_request($amount)
    {
        $ch = curl_init();
        $uniq_transaction_key='PRE'.uniqid(); //Given By Shurjumukhi Limited
        //$amount=$_POST['amount'];
        $clientIP = $_SERVER['REMOTE_ADDR'];

        $xml_data = 'spdata=<?xml version="1.0" encoding="utf-8"?>
                            <shurjoPay><merchantName>'.self::$merchant_username.'</merchantName>
                            <merchantPass>'.self::$merchant_password.'</merchantPass>
                            <userIP>'.$clientIP.'</userIP>
                            <uniqID>'.$uniq_transaction_key.'</uniqID>
                            <totalAmount>'.$amount.'</totalAmount>
                            <paymentOption>shurjopay</paymentOption>
                            <returnURL>'.self::$return_url.'</returnURL></shurjoPay>';


        $url = "https://shurjopay.shurjorajjo.com.bd/sp-data.php";
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch,CURLOPT_POSTFIELDS,$xml_data);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        print_r($response);
        curl_close ($ch);

    }

    public static function record_transactions($values)
    {
        $params = array_fill(0, count($values), '?');

        $sql = "INSERT INTO transactions VALUES(".implode(',', $params).")";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function payment_response($donor_id, $campaign_id, $org_id)
    {
        if(count($_POST)>0) {

             $response_encrypted = $_POST['spdata'];

             /*$fp = fopen('return.txt', 'a');
             $e = $response_encrypted."\n";
             fwrite($fp,$response_encrypted);
             fclose($fp);*/

             $response_decrypted = file_get_contents("https://shurjopay.shurjorajjo.com.bd/merchant/decrypt.php?data=".$response_encrypted);
             $data= simplexml_load_string($response_decrypted) or die("Error: Cannot create object");

             /*$fp = fopen('return.txt', 'a');
             $d = $response_decrypted."\n";
             fwrite($fp,$response_decrypted);
             fclose($fp);*/


             // API Information
             $tx_id = $data->txID;
             $bank_tx_id = $data->bankTxID;
             $bank_status = $data->bankTxStatus;
             $amount = $data->txnAmount;
             $sp_code = $data->spCode;
             $sp_code_des = $data->spCodeDes;
             $sp_payment_option = $data->paymentOption;
             
             // Values
             $values = array(
                 NULL,
                 $donor_id,
                 $campaign_id,
                 $org_id,
                 $amount,
                 $tx_id,
                 $bank_tx_id,
                 $bank_status,
                 $sp_code,
                 $sp_code_des,
                 $sp_payment_option,
                 date('Y-m-d')

             );

             // Record Payment Transactions
             self::record_transactions($values);

              switch($sp_code) {
                 case '000':
                     $res = array('status'=>true,'msg'=>'Success');
                     break;
                 case '100':
                     $res = array('status'=>false,'msg'=>'Transaction Decline');
                     break;
                 case '109':
                     $res = array('status'=>false,'msg'=>'Invalid Merchant');
                     break;
                 case '201':
                     $res = array('status'=>false,'msg'=>'Card Expired');
                     break;
                 case '202':
                     $res = array('status'=>false,'msg'=>'Suspected Fraud');
                     break;
                 case '300':
                     $res = array('status'=>false,'msg'=>'Action Successful');
                     break;
                 case '304':
                     $res = array('status'=>false,'msg'=>'File Edit Error');
                     break;
                 case '700':
                     $res = array('status'=>false,'msg'=>'Accepted');
                     break;
                 case '096':
                     $res = array('status'=>false,'msg'=>'Transaction Failed');
                     break;
                 case '601':
                     $res = array('status'=>false,'msg'=>'Transaction not permitted to cardholder');
                     break;
                 case '603':
                     $res = array('status'=>false,'msg'=>'Invalid Card number');
                     break;
                 default:
                     $res = array('status'=>false,'msg'=>'Unknown Error Occurred.');
                     break;
             }


             if($res['status']) {
                 //echo "Success";
                 header("location:".self::$success_url."?status=success");
                // die();
             } else {
                 //echo "Fail";
                 header("location:".self::$failed_url."?status=failed&msg=".$res['msg']);
                 //die();
             }

         }

     }

}