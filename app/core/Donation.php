<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/3/18
 * Time: 3:21 PM
 */

namespace donation;


class Donation {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }

    /* Start Money Donation */
    /////////////////    Start Donor   ////////////////////


    public static function donor_total_money_donation($donor_id)
    {
        $sql = "SELECT SUM(amount) AS amount FROM transactions WHERE donor_id =? AND sp_code=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor_id, '000'));

        $res = $stmt->fetch(2);

        return $res['amount'];
    }

    public static function donor_money_donation_contribution($donorId)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) AS contribution FROM transactions WHERE donor_id=? AND sp_code =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donorId, '000'));

        $res = $stmt->fetch(2);

        return $res['contribution'];
    }

    public static function donor_money_transaction_logs($donor_id)
    {
        $sql = "SELECT * FROM `transactions` WHERE donor_id =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor_id));

        return $stmt->fetchAll(2);
    }

    /////////////// End Donor   /////////////////////




    ///////////////////  Start NGO ///////////////////

    public static function ngo_total_money_collected($org_id)
    {
        $sql = "SELECT SUM(amount) as amount FROM transactions WHERE org_id =? AND sp_code =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id, '000'));

        $res = $stmt->fetch(2);

        return $res['amount'];
    }

    public static function total_money_donors($org_id)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) as donors FROM transactions WHERE org_id =? AND sp_code =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id, '000'));

        $res = $stmt->fetch(2);

        return $res['donors'];
    }


    public static function org_money_transaction_logs($org_id)
    {
        $sql = "SELECT * FROM `transactions` WHERE org_id =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id));

        return $stmt->fetchAll(2);
    }

    ///////////////////  End NGO ///////////////////


    /* End Money Donation */


    /* Start Goods Donation */
    public static function add_goods_donation($values)
    {
        $sql = "INSERT INTO goods (donor_id,campaign_id,org_id,category,area,location,photo,volunteer,confirm,issue_date)
                VALUES (?,?,?,?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    ///////////////////  Start NGO ///////////////////

    public static function total_goods_donors($org_id)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) as donors FROM goods WHERE org_id =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id));

        $res = $stmt->fetch(2);

        return $res['donors'];
    }

    public static function donor_goods_donation_submissions($org_id)
    {
        $sql = "SELECT * FROM `goods` WHERE org_id=? AND volunteer=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id,'None',0));

        return $stmt->fetchAll(2);
    }

    public static function volunteer_accepted_goods_list_without_confirm()
    {
        $sql = "SELECT * FROM `goods` WHERE volunteer !=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('None', 0));

        return $stmt->fetchAll(2);
    }

    public static function volunteer_goods_donation_confirm_process($goods_serial)
    {
        $sql = "UPDATE goods SET confirm=? WHERE goods_serial=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array(1, $goods_serial));
    }

    public static function volunteer_accepted_goods_list_with_confirm()
    {
        $sql = "SELECT * FROM `goods` WHERE volunteer !=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('None', 1));

        return $stmt->fetchAll(2);
    }

    ///////////////////  End NGO ///////////////////





    ///////////////////  Start Volunteer ///////////////////

    public static function total_volunteering($volunteer)
    {
        $sql = "SELECT COUNT(volunteer) AS total_volunteering FROM `goods` WHERE volunteer =? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($volunteer,1));
        $res = $stmt->fetch(2);

        return $res['total_volunteering'];
    }

    public static function volunteer_goods_collection_requests_list()
    {
        $sql = "SELECT * FROM `goods` WHERE volunteer =? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('None',0));

        return $stmt->fetchAll(2);
    }

    public static function volunteer_own_accepted_list_without_confirm($volunteer)
    {
        $sql = "SELECT * FROM `goods` WHERE volunteer =? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($volunteer, 0));

        return $stmt->fetchAll(2);
    }

    public static function volunteer_goods_collection_accept_process($volunteer, $goods_serial)
    {
        $sql = "UPDATE goods SET volunteer=? WHERE goods_serial=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($volunteer, $goods_serial));
    }

    public static function volunteer_own_accepted_list_with_confirm($volunteer)
    {
        $sql = "SELECT * FROM `goods` WHERE volunteer =? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($volunteer, 1));

        return $stmt->fetchAll(2);
    }

    ///////////////////  End Volunteer ///////////////////




    /////////////////    Start Donor   ////////////////////

    public static function donor_goods_donation_contribution($donor_id)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) AS contribution FROM goods WHERE donor_id=? AND confirm =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor_id, 1));

        $res = $stmt->fetch(2);

        return $res['contribution'];
    }

    public static function donor_goods_donations_list_without_confirm($donor)
    {
        $sql = "SELECT * FROM `goods` WHERE donor_id=? AND confirm =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor, 0));

        return $stmt->fetchAll(2);
    }

    public static function donor_goods_donations_list_with_confirm($donor)
    {
        $sql = "SELECT * FROM `goods` WHERE donor_id=? AND confirm =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor, 1));

        return $stmt->fetchAll(2);
    }
    /////////////// End Donor   /////////////////////




    /*  End Goods Donation */

}