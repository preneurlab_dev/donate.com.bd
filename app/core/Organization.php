<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:49 PM
 */

namespace org;


class Organization {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }
    
    public static function org_reg($values)
    {
        $sql = "INSERT INTO organization (org_id,org_name,org_logo,org_reg_no,org_reg_details,org_address,issue_date) VALUES (?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function org_info_update($org_info)
    {
        $sql = "UPDATE organization SET org_name=?,org_reg_no=?,org_address=? WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($org_info);
    }

    public static function logo_update($org_id)
    {
        $sql = "UPDATE organization SET org_logo=? WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($org_id);
    }

    public static function organizations()
    {
        $sql = "SELECT * FROM organization";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }
    
    public static function organization_info($organization_id)
    {
        $sql = "SELECT * FROM organization WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($organization_id));
        return $res = $stmt->fetch(2);
    }

    public static function organization_name($organization_id)
    {
        $sql = "SELECT org_name FROM organization WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($organization_id));
        $res = $stmt->fetch(2);

        return $res['org_name'];
    }

}