<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/3/18
 * Time: 1:41 PM
 */

namespace auth;

class Auth {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }

    public static function user_info($user_id)
    {
        $sql = "SELECT * FROM `users` WHERE user_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($user_id));

        return $res = $stmt->fetch(2);
    }
    
    public static function user_reg($values)
    {
        $sql = "INSERT INTO users (user_id, user_email, user_password, user_type) VALUES (?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function user_login($email,$password)
    {
        $sql = "SELECT * FROM `users` WHERE user_email=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($email));

        $user = $stmt->fetch(2);

        if($user['user_email'] == $email)
        {
            if (password_verify($password, $user['user_password']))
            {
                $_SESSION['auth'] = $user['user_id'];
                $_SESSION['type'] = $user['user_type'];

                if($_SESSION['auth'] && $_SESSION['type'])
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public static function verify_old_password($user_id, $old_password)
    {
        $sql = "SELECT user_password FROM `users` WHERE user_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($user_id));

        $res = $stmt->fetch(2);

        return (password_verify($old_password, $res['user_password'])) ? true : false;
    }

    public static function change_password($user_id, $new_password)
    {
        $sql = "UPDATE users SET user_password=? WHERE user_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($new_password, $user_id));
    }

}