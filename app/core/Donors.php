<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:49 PM
 */

namespace donors;


class Donors {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }
    
    public static function donor_reg($values)
    {
        $sql = "INSERT INTO donor (donor_id, donor_name, donor_phone, donor_photo, donor_address, issue_date) VALUES (?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function donor_info($donor_id)
    {
        $sql = "SELECT * FROM donor WHERE donor_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor_id));
        return $stmt->fetch(2);

    }

    public static function donor_name($donor_id)
    {
        $sql = "SELECT donor_name FROM donor WHERE donor_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($donor_id));
        $res = $stmt->fetch(2);

        return $res['donor_name'];
    }

    public static function personal_info_update($donor_info)
    {
        $sql = "UPDATE donor SET donor_name=?,donor_phone=?,donor_address=? WHERE donor_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($donor_info);
    }

    public static function photo_update($donor_info)
    {
        $sql = "UPDATE donor SET donor_photo=? WHERE donor_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($donor_info);
    }
}