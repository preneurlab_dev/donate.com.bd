<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 7/5/18
 * Time: 1:57 PM
 */

namespace email_subscriptions;


class EmailSubscriptions {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }

    public static function check_subscription_email_exists($email)
    {
        $sql = "SELECT * FROM `subscriptions` WHERE email=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($email));

        if($stmt->rowCount())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function add_subscription_email($email)
    {
        $sql = "INSERT INTO subscriptions(email) VALUES(?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($email));
    }

}