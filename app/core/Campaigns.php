<?php
/**
 * Created by PhpStorm.
 * User: hacker
 * Date: 6/4/18
 * Time: 2:55 AM
 */

namespace campaigns;


class Campaigns {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }
    
    public static function add_campaign($values)
    {
        $sql = "INSERT INTO campaigns (campaign_id,campaign_title,category,campaign_desc,org_id,campaign_photo,campaign_start,campaign_end,campaign_status,target_money,issue_date) 
        VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function campaign_edit($values)
    {
        $sql = "UPDATE campaigns SET campaign_title=?,campaign_desc=?,campaign_photo=?,campaign_start=?,campaign_end=?,target_money=? WHERE campaign_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function campaign_delete($campaign_id)
    {
        $sql = "DELETE FROM campaigns WHERE campaign_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array($campaign_id));
    }

    public static function campaign_completed($campaign_id)
    {
        $sql = "UPDATE `campaigns` SET campaign_status=? WHERE campaign_id =?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute(array('End', $campaign_id));

    }


    public static function campaigns()
    {
        $sql = "SELECT * FROM campaigns WHERE campaign_status !=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('End'));

        return $stmt->fetchAll(2);
    }

    public static function index_campaigns()
    {
        $sql = "SELECT * FROM campaigns WHERE campaign_status !=? LIMIT 6";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array('End'));

        return $stmt->fetchAll(2);
    }

    public static function org_campaign_category($org_id)
    {
        $sql = "SELECT DISTINCT category FROM campaigns WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id));

        return $stmt->fetchAll(2);
    }

    public static function campaigns_category()
    {
        $sql = "SELECT DISTINCT category FROM campaigns";
        $stmt = self::$db->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(2);
    }

    public static function org_campaigns_with_category($org_id,$category)
    {
        $sql = "SELECT * FROM campaigns WHERE org_id =? AND category=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id,$category));

        return $stmt->fetchAll(2);
    }

    public static function campaigns_with_category($category)
    {
        $sql = "SELECT * FROM campaigns WHERE category=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($category));

        return $stmt->fetchAll(2);
    }

    public static function org_campaigns_all($org_id)
    {
        $sql = "SELECT * FROM campaigns WHERE org_id =?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id));

        return $stmt->fetchAll(2);
    }

    public static function ngo_campaigns_count($org_id)
    {
        $sql = "SELECT COUNT(DISTINCT campaign_id) AS campaign_count FROM campaigns WHERE org_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($org_id));

        $res = $stmt->fetch(2);

        return $res['campaign_count'];
    }

    public static function campaign_title_by_id($campaign_id)
    {
        $sql = "SELECT campaign_title FROM campaigns WHERE campaign_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id));
        $res = $stmt->fetch(2);

        return $res['campaign_title'];
    }

    public static function campaign_info_by_id($campaign_id)
    {
        $sql = "SELECT * FROM campaigns WHERE campaign_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id));
        return $stmt->fetch(2);
    }

    public static function campaign_owner_by_campaign_id($campaign_id)
    {
        $sql ="SELECT org_id FROM campaigns WHERE campaign_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id));

        $res= $stmt->fetch(2);

        return $res['org_id'];
    }

    public static function campaign_money_earned($campaign_id)
    {
        $sql = "SELECT SUM(sent_amount) AS earned FROM money WHERE campaign_id=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id, 1));
        $res = $stmt->fetch(2);

        return $res['earned'];
    }

    public static function campaign_money_donors($campaign_id)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) as money_contributors FROM money WHERE campaign_id=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id, 1));
        $res = $stmt->fetch(2);

        return $res['money_contributors'];
    }

    public static function campaign_goods_donors($campaign_id)
    {
        $sql = "SELECT COUNT(DISTINCT donor_id) as goods_contributors FROM goods WHERE campaign_id=? AND confirm=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($campaign_id, 1));
        $res = $stmt->fetch(2);

        return $res['goods_contributors'];
    }

}