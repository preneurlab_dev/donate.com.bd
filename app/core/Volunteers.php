<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/9/18
 * Time: 5:49 PM
 */

namespace volunteers;


class Volunteers {

    private static $db;

    public static function db_config($config)
    {
        self::$db = $config;
    }
    
    public static function volunteer_reg($values)
    {
        $sql = "INSERT INTO volunteer (volunteer_id, volunteer_name, volunteer_phone, volunteer_photo, volunteer_area, volunteer_address, issue_date) VALUES (?,?,?,?,?,?,?)";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($values);
    }

    public static function volunteer_info($volunteer_id)
    {
        $sql = "SELECT * FROM volunteer WHERE volunteer_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($volunteer_id));
        return $stmt->fetch(2);

    }

    public static function volunteer_name($volunteer_id)
    {
        $sql = "SELECT volunteer_name FROM volunteer WHERE volunteer_id=?";
        $stmt = self::$db->prepare($sql);
        $stmt->execute(array($volunteer_id));
        $res = $stmt->fetch(2);

        return $res['volunteer_name'];
    }

    public static function personal_info_update($volunteer_id)
    {
        $sql = "UPDATE volunteer SET volunteer_name=?,volunteer_phone=?,volunteer_address=? WHERE volunteer_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($volunteer_id);
    }

    public static function photo_update($volunteer_info)
    {
        $sql = "UPDATE volunteer SET volunteer_photo=? WHERE volunteer_id=?";
        $stmt = self::$db->prepare($sql);
        return $stmt->execute($volunteer_info);
    }

}