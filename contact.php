<?php

require_once 'app/fun/functions.php';
require_once 'app/campaign-controller.php';

$campaigns = campaigns();

?>

    <?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
<style>
        .welcomte-content {
            width: 100%;
            height: 350px;
        }
</style>
    <!--    [ Strat Section Title Area]-->
    <section id="welcome" class="">
        <div class="overlay">
            <div class="wel-come-areas owl-carousel">
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Connect. <span>Donate.</span> Impact.</h2>
                                    <h5>Your time to make a difference.</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg-02 d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Verify. <span>Donate.</span> Validate.</h2>
                                    <h5>How many lives you can change today ?</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->



    <!--    [ Strat Section Area]-->
    <section id="mission">
        <div class="section-padding">
            <div class="container">
                

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>Contact</h4>
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d378.1749653313493!2d90.38096122965273!3d23.737845913116857!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8c9c6829b67%3A0x58fec4edbf6db8bd!2sBel+Tower+(Beximco)!5e0!3m2!1sen!2sbd!4v1529695840499" width="1000" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                           <p>Address: Preneur Lab Limited, 4th Floor, House 519, Road 1, Dhanmondi, Dhaka.</p>
                                <p>e-mail: mail@preneurlab.com || Phone: +8801710950929</p>
                            </div>
                            <div class="mission-txt">
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'footer.php'; ?>
