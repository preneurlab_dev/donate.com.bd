<?php
/**
 * Created by PhpStorm.
 * User: sayed
 * Date: 6/3/18
 * Time: 11:55 AM
 */

if(isset($_POST['submit']))
{
    if(is_uploaded_file($_FILES['image']['tmp_name']))
    {
        $file_info = new finfo(FILEINFO_MIME_TYPE);

        $fileContents = file_get_contents($_FILES['image']['tmp_name']);
        $mimeType = $file_info->buffer($fileContents);
        $allowed_image_type = array('image/gif', 'image/png', 'image/jpeg');

        if(in_array($mimeType, $allowed_image_type))
        {
            echo 'allowed';
        }
        else
        {
            echo 'invalid';
        }
    }
    else{
        echo 'Not Uploaded';
    }

}

?>

<form action="check.php" method="post" enctype="multipart/form-data">
    <input type="file" name="image">
    <input type="submit" name="submit" value="Upload">
</form>
