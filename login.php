<?php

require_once 'app/fun/functions.php';
require_once 'app/autoload.php';

// Check User Login Authentication
if (isset($_POST['login']))
{
    if(user_login() == true)
    {
        $url = 'index.php';

        if(isset($_COOKIE['_link']))
        {
            $url = !empty($_COOKIE['_link']) ? decryption($_COOKIE['_link']) : 'index.php';
        }
        header("Location: ".$url);
    }
    elseif(user_login() == false)
    {
        header("Location: login.php?msg=error");
    }
}
else
{
    // Redirect to User Panel if Logged In
    logged_in_redirect();
}


?>

<?php set_page_title('donate.com.bd | Login'); require_once 'header.php'; ?>


<style>
    .single-btn a {
        display: block;
        color: #49BA6F;
        margin-top: 15px;
        font-weight: bold;
    }
</style>
<section id="login">
    <div class="section-padding">
        <div class="container">

            <?php if(isset($_GET['msg']) && $_GET['msg'] == 'error'): ?>
            <div class="row justify-content-center message">
                <div class="col-lg-4">
                    <div class="alert alert-warning alert-dismissible fade show text-center message" role="alert">
                        <strong>Warning:</strong> Incorrect email or password.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="log-frm">
                        <form action="login.php" class="needs-validation" novalidate method="post" enctype="multipart/form-data">
                            <div class="card">
                                <div class="card-header">
                                    <div class="log-logo"><img src="assets/img/logo.png" alt=""></div>
                                    <div class="log-txt text-right">
                                        <h6>Login Panel</h6>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="single-form">
                                        <input name="email" class="form-control" id="email" type="email" placeholder="Email address" required>
                                        <div class="invalid-feedback">
                                            Email address is required.
                                        </div>
                                    </div>
                                    <div class="single-form">
                                        <input name="password" class="form-control" id="phone" type="password" placeholder="Password" required>
                                        <div class="invalid-feedback">
                                            Password is required.
                                        </div>
                                    </div>
                                    <div class="single-btn">
                                        <button type="submit" name="login">Login</button>
                                    </div>
                                    <div class="single-btn">
                                        <a href="registration/index.php">Register Here</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require('footer.php'); ?>

<script>
    $( document ).ready(function() {
        $('.message').fadeOut(3000);
    });
</script>
