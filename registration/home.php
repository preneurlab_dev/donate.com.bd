<?php

require_once '../app/fun/functions.php';

?>

    <?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
    <style>
      
    </style>
    <!--    [ Strat Registration Area]-->
    <section id="regintration">
        <div class="section-padding">
            <div class="container">
               <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <a href="donor.php" class="single-reg-link">
                            <i class="icofont icofont-unity-hand"></i>
                            <h4>Donor Registration</h4>
                        </a>
                    </div>
                    <div class="col-lg-6 text-center">
                        <a href="volunteer.php" class="single-reg-link">
                            <i class="icofont icofont-tree-alt"></i>
                            <h4>Volunteer Registration</h4>
                        </a>
                    </div>
                    <div class="col-lg-6 text-center">
                        <a href="organization.php" class="single-reg-link">
                            <i class="icofont icofont-triangle"></i>
                            <h4>NGO Registration</h4>
                        </a>
                    </div>
                </div>
                <a href="../index.php" class="back-btn"><i class="icofont icofont-long-arrow-left"></i></a>
            </div>
        </div>
    </section>
    <!--    [Finish Registration Area]-->
    <?php require_once 'footer.php'; ?>
