<?php

require_once '../app/fun/functions.php';
require_once '../app/autoload.php';

$reg_status = false;

if(isset($_POST['org_reg']))
{
    $reg_status = org_reg();

    if($reg_status === true)
    {
        header('Location: organization.php?msg=success');
    }
    elseif ($reg_status === 'empty_fields')
    {
        header('Location: organization.php?msg=empty');
    }
    elseif($reg_status === 'invalid_mime')
    {
        header('Location: organization.php?msg=invalid_mime');
    }
}


?>

    <?php set_page_title('Organization | Registration'); require_once 'header.php'; ?>
     <style>
        .single-form button{
            width:86%;
        }
    </style>
    <!--    [ Strat Registration Area]-->
    <section id="regintration">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 m-md-auto text-center">
                        <div class="section-title bg-dark-title">
                            <h2>Organization Registration</h2>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'success'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <strong>Success:</strong> Thank You For Registration. &nbsp; <a href="../login.php" class="alert-link">Login</a>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'empty'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Please Fill All The Fields.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'invalid_mime'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Only allowed photo types are : <strong>png, jpeg and gif</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>


                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="reg-frm">
                            <div class="card">
                                <form action="organization.php" class="needs-validation" novalidate method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="org_name">Org. Name*</label>
                                                <input name="org_name" class="form-control" id="org_name" type="text" placeholder="Org. name" required>
                                                <div class="invalid-feedback">
                                                    Please enter organization name.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="org_email">Email Address*</label>
                                                <input name="org_email" class="form-control" id="org_email" type="email" placeholder="Email address" required>
                                                <div class="invalid-feedback">
                                                    Please enter organization email.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="org_reg_no">Org. Reg. Number*</label>
                                                        <input name="org_reg_no" class="form-control" id="org_reg_no" type="text" placeholder="Registration number" required>
                                                        <div class="invalid-feedback">
                                                            Please enter your org. reg. number.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="org_pass">Password*</label>
                                                        <input name="org_pass" class="form-control" id="org_pass" type="password" placeholder="Enter a password" required>
                                                        <div class="invalid-feedback">
                                                            Please organization password.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="org_logo">Org. Logo*</label>
                                                        <button><i class="icofont icofont-image"></i></button>
                                                        <input name="org_logo" class="form-control" id="org_logo" type="file" required>
                                                        <div class="invalid-feedback">
                                                            Please upload org. logo.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="single-form">
                                                <label for="org_reg_details">Org. Reg. Details*</label>
                                                <textarea name="org_reg_details" class="form-control" id="org_reg_details" placeholder="Registration Authority and Details" required></textarea>
                                                <div class="invalid-feedback">
                                                    Please enter organization reg. details.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="single-form">
                                                <label for="org_address">Org. Address*</label>
                                                <textarea name="org_address" class="form-control" id="org_address" placeholder="Organization Address" required></textarea>
                                                <div class="invalid-feedback">
                                                    Please enter org. address.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-4">
                                            <div class="reg-btn">
                                                <button type="submit" name="org_reg">Register</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="index.php" class="back-btn"><i class="icofont icofont-long-arrow-left"></i></a>
            </div>
        </div>
    </section>
    <!--    [Finish Registration Area]-->
    <?php require_once 'footer.php'; ?>
