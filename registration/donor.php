<?php

require_once '../app/fun/functions.php';
require_once '../app/autoload.php';

$reg_status = false;

if(isset($_POST['donor_reg']))
{
    $reg_status = donor_reg();

    if($reg_status === true)
    {
        header('Location: donor.php?msg=success');
    }
    elseif ($reg_status === 'empty_fields')
    {
        header('Location: donor.php?msg=empty');
    }
    elseif($reg_status === 'invalid_mime')
    {
        header('Location: donor.php?msg=invalid_mime');
    }
}

?>

    <?php set_page_title('Donor | Registration'); require_once 'header.php'; ?>
    <style>
        .single-form button{
            width:86%;
        }
    </style>
    <!--    [ Strat Registration Area]-->
    <section id="regintration">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 m-md-auto text-center">
                        <div class="section-title bg-dark-title">
                            <h2>Donor Registration</h2>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'success'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <strong>Success:</strong> Thank you for registration  &nbsp; <a href="../login.php" class="alert-link">Login</a>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'empty'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Please fill all the fields correctly.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'invalid_mime'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Only allowed photo types are : <strong>png, jpeg and gif</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>


                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="reg-frm">
                            <div class="card">
                                <form action="donor.php" class="needs-validation" novalidate method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="donor_name">Full Name*</label>
                                                <input name="donor_name" class="form-control" id="donor_name" type="text" placeholder="Full name" required>
                                                <div class="invalid-feedback">
                                                    Please enter your full name.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="donor_email">Email*</label>
                                                <input name="donor_email" class="form-control" id="donor_email" type="email" placeholder="Email address" required>
                                                <div class="invalid-feedback">
                                                    Please enter your email.
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="donor_phone">Contact Number*</label>
                                                        <input name="donor_phone" class="form-control" id="donor_phone" type="text" placeholder="Contact number" required>
                                                        <div class="invalid-feedback">
                                                            Please enter your contact number.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="donor_pass">Password*</label>
                                                        <input name="donor_pass" class="form-control" id="donor_pass" type="password" placeholder="Enter a password" required>
                                                        <div class="invalid-feedback">
                                                            Please enter your password.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="single-form">
                                                        <label for="donor_photo">Upload Photo*</label>
                                                        <button><i class="icofont icofont-image"></i></button>
                                                        <input name="donor_photo" class="form-control" id="donor_photo" type="file" required>
                                                        <div class="invalid-feedback">
                                                            Please upload your photo.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="single-form">
                                                <label for="donor_address">Address*</label>
                                                <textarea name="donor_address" class="form-control" id="donor_address" placeholder="Your present address" required></textarea>
                                                <div class="invalid-feedback">
                                                    Please enter address
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-4">
                                            <div class="reg-btn">
                                                <button type="submit" name="donor_reg">Register</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="index.php" class="back-btn"><i class="icofont icofont-long-arrow-left"></i></a>
            </div>
        </div>
    </section>
    <!--    [Finish Registration Area]-->
    <?php require_once 'footer.php'; ?>
