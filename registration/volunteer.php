<?php

require_once '../app/fun/functions.php';
require_once '../app/autoload.php';

$reg_status = false;

if(isset($_POST['volunteer_reg']))
{
    $reg_status = volunteer_reg();

    if($reg_status === true)
    {
        header('Location: volunteer.php?msg=success');
    }
    elseif ($reg_status === 'empty_fields')
    {
        header('Location: volunteer.php?msg=empty');
    }
    elseif($reg_status === 'invalid_mime')
    {
        header('Location: volunteer.php?msg=invalid_mime');
    }
}

?>

    <?php set_page_title('Volunteer | Registration'); require_once 'header.php'; ?>
    <style>
        .single-form select {
            width: 100%;
            height: 38px;
            padding: 0 10px;
            margin-bottom: 10px;
        }
        .single-form button{
            width: 86%;
        }
    </style>
    <!--    [ Strat Registration Area]-->
    <section id="regintration">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 m-md-auto text-center">
                        <div class="section-title bg-dark-title">
                            <h2>Volunteer | Registration</h2>
                        </div>
                    </div>
                </div>

                <div class="row justify-content-center">
                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'success'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <strong>Success:</strong> Thank You For Registration.  &nbsp; <a href="../login.php" class="alert-link">Login</a>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'empty'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Please Fill All The Fields.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'invalid_mime'): ?>
                        <div class="col-lg-7">
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Only allowed photo types are : <strong>png, jpeg and gif</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>

                <div class="row justify-content-center">
                    <div class="col-lg-7">
                        <div class="reg-frm">
                            <div class="card">
                                <form action="volunteer.php" class="needs-validation" novalidate method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="volunteer_name">Full Name*</label>
                                                <input name="volunteer_name" class="form-control" id="volunteer_name" type="text" placeholder="Full name" required>
                                                <div class="invalid-feedback">
                                                    Please enter your full name.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="volunteer_phone">Contact Number*</label>
                                                <input name="volunteer_phone" class="form-control" id="volunteer_phone" type="text" placeholder="Contact number" required>
                                                <div class="invalid-feedback">
                                                    Please enter your contact number.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="volunteer_pass">Password*</label>
                                                <input name="volunteer_pass" class="form-control" id="volunteer_pass" type="password" placeholder="Enter Your Password" required>
                                                <div class="invalid-feedback">
                                                    Please enter your password.
                                                </div>
                                            </div>
                                        </div>
                                         <div class="col-lg-6">
                                            <div class="single-form">
                                                <label for="volunteer_email">Email*</label>
                                                <input name="volunteer_email" class="form-control" id="volunteer_email" type="email" placeholder="Email address" required>
                                                <div class="invalid-feedback">
                                                    Please enter your email.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="area">Your Area*</label>
                                                <select name="volunteer_area" class="form-control" id="area">
                                                  <option value="">Select Area</option>
                                                  <option value="Dhanmondi">Dhanmondi</option>
                                                  <option value="Mirpur">Mirpur</option>
                                                  <option value="Gulshan">Gulshan</option>
                                                  <option value="Old Dhaka (Bangshal)">Old Dhaka (Bangshal)</option>
                                                  <option value="Old Dhaka (Wari)">Old Dhaka (Wari)</option>
                                                  <option value="Dhaka University">Dhaka University</option>
                                                  <option value="Chattogram">Chattogram</option>
                                                  <option value="Rangpur">Rangpur</option>
                                                  <option value="Khulna">Khulna</option>
                                                  <option value="Barisal">Barisal</option>
                                                </select>
                                                <div class="invalid-feedback">
                                                    Please enter your area.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="volunteer_photo">Upload Photo*</label>
                                                <button><i class="icofont icofont-image"></i></button>
                                                <input name="volunteer_photo" class="form-control" id="volunteer_photo" type="file" required>
                                                <div class="invalid-feedback">
                                                    Please upload your photo.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                                <div class="col-lg-4">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="col-lg-12">
                                            <div class="single-form">
                                                <label for="volunteer_address">Address*</label>
                                                <textarea name="volunteer_address" class="form-control" id="volunteer_address" placeholder="Your present address" required></textarea>
                                                <div class="invalid-feedback">
                                                    Please enter your address.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-lg-4">
                                            <div class="reg-btn">
                                                <button type="submit" name="volunteer_reg">Register</button>
                                            </div> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="index.php" class="back-btn"><i class="icofont icofont-long-arrow-left"></i></a>
            </div>
        </div>
    </section>
    <!--    [Finish Registration Area]-->
    <?php require_once 'footer.php'; ?>
