<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>
        <?php get_page_title() ?>
    </title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="../assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="../assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="../assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="../assets/css/style.css">
    <link rel="stylesheet" href="../assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <!--<div class="pre-loader-area">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>-->


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <div class="header-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header-call">
                            <a href="tel:+8801825573355"><i class="icofont icofont-phone"></i> +8801825573355</a>
                            <a href="tel:+8801913366632"><i class="icofont icofont-phone"></i> +8801913366632</a>
                        </div>
                    </div>
                    <div class="col-lg-6 text-right">
                        <div class="top-social">
                            <a href="https://www.facebook.com/donate.com.bd/"><i class="icofont icofont-social-facebook"></i></a>
                            <!--<a href="#"><i class="icofont icofont-social-twitter"></i></a>-->
                            <!--<a href="#"><i class="icofont icofont-brand-youtube"></i></a>-->
                            <!--<a href="#"><i class="icofont icofont-social-google-plus"></i></a>-->
                            <!--<a href="#"><i class="icofont icofont-social-dribbble"></i></a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--    [ Strat Logo Area]-->
        <div class="main-menu" data-sticky>
            <nav class="navbar navbar-light navbar-expand-md bg-light justify-content-between">
                <a class="navbar-brand" href="../index.php">
                    <div class="logo">
                        <img src="../assets/img/logo.png" alt="">
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsingNavbar2" data-sticky> <span class="navbar-toggler-icon"></span></button>
                <div class="navbar-collapse collapse justify-content-between" id="collapsingNavbar2">
                    <div>
                        <!--placeholder to evenly space flexbox items and center links-->
                    </div>
                    <ul class="navbar-nav">
                        <li class="nav-ite">
                            <a class="nav-link" href="../index.php">Home</a>
                        </li>
                
                        <li class="nav-item">
                            <a class="nav-link" href="../how.php">How It Works</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../trust.php">Why You Trust Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../registration/index.php">Register</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../contact.php">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="../terms-and-privacy.php">Terms & Condition and Privacy Policy</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav flex-row">
                        <li class="nav-item">
                            <a class="nav-link" href="../login.php"><?php (isset($_SESSION['auth']) ? print 'Dashboard' : print 'Login')?></a>
                        </li>

                        <?php if(isset($_SESSION['auth']) && $_SESSION['type']!='donor'): ?>

                            <li class="nav-item">
                                <a class="nav-link" href="../logout.php">Logout</a>
                            </li>
                        <?php else: ?>

                            <li class="nav-item theme-btn">
                                <a class="nav-link" href="../donation/index.php">Donate Now</a>
                            </li>

                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->