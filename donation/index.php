<?php

require_once '../app/fun/functions.php';

?>

<?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
<style>

</style>
<!--    [ Strat Registration Area]-->
<section id="regintration">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 text-center">
                    <a href="money.php<?php !empty($_GET['campaign']) ? print '?campaign='.$_GET['campaign'] : print null?>" class="single-reg-link">
                        <i class="icofont icofont-money-bag"></i>
                        <h4>Donate Money</h4>
                    </a>
                </div>
                <div class="col-lg-6 text-center">
                    <a href="goods.php<?php !empty($_GET['campaign']) ? print '?campaign='.$_GET['campaign'] : print null?>" class="single-reg-link">
                        <i class="icofont icofont-delete-alt"></i>
                        <h4>Donate Goods</h4>
                    </a>
                </div>
            </div>
            <a href="../index.php" class="back-btn"><i class="icofont icofont-long-arrow-left"></i></a>
        </div>
    </div>
</section>


<!--    [Finish Registration Area]-->
<?php require_once 'footer.php'; ?>
