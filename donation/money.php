<?php

require_once '../app/fun/functions.php';
require_once '../app/autoload.php';

$campaign_title = campaign_title_by_id($_GET['campaign']);

$campaigns = campaigns();
$organizations = organizations();

// Page redirect after login
redirect_page('donation/money.php');

// Redirect If User Not Logged In
unauthorized_user_redirect('../login.php');


if(isset($_SESSION['auth']) && $_SESSION['type']!='donor')
{
    header('Location: ../donate-info.php');
}

if(isset($_POST['money']))
{
    $payment_request_status = payment_request();

    if($payment_request_status === 'empty_fields')
    {
        header('Location: money.php?msg=empty');
    }
}

?>

<?php set_page_title('Donate Money | donate.com.bd'); require_once 'header.php'; ?>


<section id="single-money">
    <div class="section-padding-">
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-lg-8">

                    <div class="mission-content mission-content-01">
                        <div class="card">

                            <?php if(isset($_GET['msg'])): ?>

                                <div class="alert alert-warning alert-dismissible fade show text-center message" role="alert">
                                    <strong>Warning:</strong> Fields can not be empty
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                            <?php endif; ?>

                            <div class="mission-title text-center">
                                <h4><i class="icofont icofont-money-bag"></i> Donate Money </h4>
                            </div>
                            <div class="donate-frm">
                                <form action="money.php" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                                    <div class="row">
                                        <div class="col-lg-6">

                                            <div class="singleofild">
                                                <div class="gide-line">
                                                    <p>
                                                        <strong>Pay with bKash Guides</strong>
                                                        <br><strong>bKash Wallet: </strong> 01845032741
                                                        <br><strong>Wallet Method: </strong> Payment (Option 3)
                                                        <br>First Pay with your mobile.
                                                        <br>Collect Mobile Message TrxID.
                                                        <br>Use TrxID to next step.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="singleofild-radio">

                                                <?php if(!isset($_GET['campaign']) && empty($_GET['campaign'])): ?>

                                                    <label>Preferred Org. Or Campaign*</label>

                                                    <div class="single-fild-part">
                                                        <input type="radio" name="preferred" class="preferred_org" id="preferred_org" required><label for="preferred_org">Organization</label>
                                                    </div>
                                                    <div class="single-fild-part">
                                                        <input type="radio" name="preferred" class="preferred_campaign" id="preferred_campaign" required><label for="preferred_campaign">Campaign</label>
                                                    </div>
                                                    <select name="campaign_id" style="display: none" class="campaign">
                                                        <option value="">Select Campaign</option>

                                                        <?php foreach ($campaigns as $campaign): ?>
                                                            <option value="<?php echo $campaign['campaign_id']?>"><?php echo $campaign['campaign_title'] ?></option>
                                                        <?php endforeach; ?>

                                                    </select>

                                                    <select name="org_id" style="display: none" class="organization">
                                                        <option value="">Select Organization</option>

                                                        <?php foreach ($organizations as $organization): ?>
                                                            <option value="<?php echo $organization['org_id']?>"><?php echo $organization['org_name'] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>

                                                <?php else: ?>
                                                    <label for="campaign_id">Preferred Org. Or Campaign*</label>
                                                    <h6><?php echo $campaign_title; ?></h6>
                                                    <input type="hidden" name="campaign_id" value="<?php echo $_GET['campaign'] ?>">
                                                <?php endif; ?>

                                            </div>

                                            <div class="singleofild">
                                                <label for="amount">Amount*</label>
                                                <input type="text" name="amount" class="form-control" id="amount" placeholder="Enter amount" required>
                                                <div class="invalid-feedback">
                                                    Please enter amount.
                                                </div>
                                            </div>

                                            <div class="turmcondition">

                                                <p> <span><input type="checkbox" required></span>By clicking Confirm, you agree to our <a href="../terms-and-privacy.php">Terms & Condition and Privacy Policy.</a> Please read before confirm.</p>
                                            </div>

                                            <div class="singleofild">
                                                <button type="submit" name="money">Confirm</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <div class="row justify-content-center">
                                    <div class="col-lg-12">
                                        <div class="payment-logos">
                                            <img src="../assets/img/001.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php require('footer.php'); ?>

<script>

    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $(document).ready(function() {
        $('.preferred_org').click(function() {
            $('.campaign').css('display', 'none');
            $('.organization').css('display', 'block');
        });

        $('.preferred_campaign').click(function() {
            $('.campaign').css('display', 'block');
            $('.organization').css('display', 'none');
        });


    });

/*    function stopRKey(evt) {
        var evt = (evt) ? evt : ((event) ? event : null);
        var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
        if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
    }

    document.onkeypress = stopRKey;*/




</script>