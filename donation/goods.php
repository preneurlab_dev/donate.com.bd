<?php

require_once '../app/fun/functions.php';
require_once '../app/donation-controller.php';
require_once '../app/campaign-controller.php';
require_once '../app/org-controller.php';

$add_transaction_success = false;
$add_goods_success = false;
$campaign_title = campaign_title_by_id($_GET['campaign']);

$campaigns = campaigns();
$organizations = organizations();

// Page redirect after login
redirect_page('donation/goods.php');

// Redirect If User Not Logged In
unauthorized_user_redirect('../login.php');

if(isset($_SESSION['auth']) && $_SESSION['type']!='donor')
{
    header('Location: ../donate-info.php');
}

if(isset($_POST['donate_goods']))
{
    if(add_goods_donation() == true)
    {
        header('Location: goods.php?msg=success');
    }
    else
    {
        header('Location: goods.php?msg=empty');
    }
}
?>

<?php set_page_title('Donate Goods'); require_once 'header.php'; ?>

<style>
    .singleofild.photo input{
        opacity: 1;
        padding-left: 0;
    }
    .singleofild button {
        margin-top: 55px;
    }
    .singleofild-radio {
        margin-top: 0px;
    }
</style>
<section id="single-money">
    <div class="section-padding-2">
        <div class="container">


            <?php if(isset($_GET['msg']) && $_GET['msg']== 'success'): ?>
                <div class="row justify-content-center message">
                    <div class="col-lg-8">
                        <div class="alert alert-success alert-dismissible fade show text-center message" role="alert">
                            <strong>Success:</strong> Thank you for donation.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(isset($_GET['msg']) && $_GET['msg']== 'empty'): ?>
                <div class="row justify-content-center message">
                    <div class="col-lg-8">
                        <div class="alert alert-warning alert-dismissible fade show text-center message" role="alert">
                            <strong>Warning:</strong> Please fill the fields correctly.
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="mission-content mission-content-01">
                        <div class="card">
                            <div class="mission-title text-center">
                                <h4><i class="icofont icofont-box"></i> Donate Goods</h4>
                            </div>
                            <div class="mission-txt">
                                <div class="donate-frm">
                                    <form action="goods.php" enctype="multipart/form-data" method="post">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="singleofild">
                                                    <div class="frm-pic">
                                                        <img id="test" src="../assets/img/dp/06.jpg">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="singleofild-radio">
                                                    <?php if(!isset($_GET['campaign']) && empty($_GET['campaign'])): ?>
                                                        <p>Preferred Org. Or Campaign*</p>
                                                        <div class="single-fild-part">
                                                            <input type="radio" name="preferred" class="preferred_org" required><label>Organization</label>
                                                        </div>
                                                        <div class="single-fild-part">
                                                            <input type="radio" name="preferred" class="preferred_campaign" required><label>Campaign</label>
                                                        </div>
                                                        <select name="campaign_id" style="display: none" class="campaign">
                                                            <option value="">Select Campaign</option>

                                                            <?php foreach ($campaigns as $campaign): ?>
                                                                <option value="<?php echo $campaign['campaign_id']?>"><?php echo $campaign['campaign_title'] ?></option>
                                                            <?php endforeach; ?>

                                                        </select>


                                                        <select name="org_id" style="display: none" class="organization">
                                                            <option value="">Select Organization</option>

                                                            <?php foreach ($organizations as $organization): ?>
                                                                <option value="<?php echo $organization['org_id']?>"><?php echo $organization['org_name'] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>


                                                    <?php else: ?>
                                                        <p>Your Preferred Campaign*</p>
                                                        <h6><?php echo $campaign_title; ?></h6>
                                                        <input type="hidden" name="campaign_id" value="<?php echo ($_GET['campaign']) ?>">
                                                    <?php endif; ?>
                                                </div>
                                                <div class="singleofild">
                                                    <label for="category">Category</label>
                                                    <select name="category" id="category">
                                                        <option value="">Select Category</option>
                                                        <option value="Food">Food</option>
                                                        <option value="Cloth">Cloth</option>
                                                        <option value="Books">Books</option>
                                                        <option value="Others">Others</option>
                                                    </select>
                                                </div>
                                                <div class="singleofild">
                                                    <label for="">Your Area</label>
                                                    <select name="area">
                                                        <option value="Area">Area</option>
                                                        <option value="Dhanmondi">Dhanmondi</option>
                                                        <option value="Mirpur">Mirpur</option>
                                                        <option value="Gulshan">Gulshan</option>
                                                        <option value="Old Dhaka (Bangshal)">Old Dhaka (Bangshal)</option>
                                                        <option value="Old Dhaka (Wari)">Old Dhaka (Wari)</option>
                                                        <option value="Dhaka University">Dhaka University</option>
                                                        <option value="Chattogram">Chattogram</option>
                                                        <option value="Rangpur">Rangpur</option>
                                                        <option value="Khulna">Khulna</option>
                                                        <option value="Barisal">Barisal</option>
                                                    </select>
                                                </div>
                                                <div class="singleofild">
                                                    <label for="location">Address / Pickup point for Volunteer</label>
                                                    <textarea name="location" id="location" cols="30" rows="2" placeholder="Location" required></textarea>
                                                </div>
                                                <div class="singleofild photo">
                                                    <label for="">Your Goods Photo</label>
                                                    <!-- <button>Upload<i class="icofont icofont-image"></i></button>-->
                                                    <input id="tstpic" name="goods_photo" id="goods_photo" type="file" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-center">
                                            <div class="col-lg-3">
                                                <div class="singleofild">
                                                    <button type="submit" name="donate_goods">Confirm</button>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <!--
                                <div class="turm-condition">
                                    <div class="card">
                                        <p>* Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, maiores?</p>
                                        <p>* Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi, maiores?</p>
                                    </div>
                                </div>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php require('footer.php'); ?>

<script>
    $(document).ready(function() {
        $('.preferred_org').click(function() {
            $('.campaign').css('display', 'none');
            $('.organization').css('display', 'block');
        })

        $('.preferred_campaign').click(function() {
            $('.campaign').css('display', 'block');
            $('.organization').css('display', 'none');
        })
    })

</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#test').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#tstpic").change(function() {
            readURL(this);
        }

    );

</script>