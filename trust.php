<?php

require_once 'app/fun/functions.php';

?>

    <?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
<style>
    .welcomte-content {
        width: 100%;
        height: 350px;
    }
</style>
    <!--    [ Strat Section Title Area]-->
    <section id="welcome" class="">
        <div class="overlay">
            <div class="wel-come-areas owl-carousel">
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Connect. <span>Donate.</span> Impact.</h2>
                                    <h5>Your time to make a difference.</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg-02 d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Verify. <span>Donate.</span> Validate.</h2>
                                    <h5>How many lives you can change today ?</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->



    <!--    [ Strat Section Area]-->
    <section id="mission">
        <div class="section-padding">
            <div class="container">
                

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>Trusted Solution.</h4>
                                 <img src="assets/img/donate_trust.png"/>
                            </div>
                            <div class="mission-txt">
                                <p>Donate.com.bd is a fully transparent and trusted system. Donors and organizations can see the donation amount is real time. Donors, volunteers(when engaged) and organizations receive notifications in every steps. Payments are handled by ShurjoPay a Banglaldesh Bank approved entity. The payments are distributed in every 15 days in the bank accunts of the organizations. All campaigns and organizations are verified before made public. All user data is sent to server in entrypted form (over SSL) so the data can't be hacked or seen in the middle. The system is "Blockchain Ready". This makes the system easyli transfered or connected into a blockchain based network for further transparency.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'footer.php'; ?>
