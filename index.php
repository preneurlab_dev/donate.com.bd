<?php

require_once 'app/fun/functions.php';
require_once 'app/campaign-controller.php';
require_once 'app/org-controller.php';
require_once 'app/email-subscription-controller.php';

if(isset($_POST['subscribe']))
{
    if(isset($_POST['subscribe_email']))
    {
        $subscription_res = add_subscription_email($_POST['subscribe_email']);

        if($subscription_res === true)
        {
            echo '<script>alert("Subscription is successful.")</script>';
        }
        elseif($subscription_res == 'already_exists')
        {
            echo '<script>alert("Your email has subscribe already.")</script>';
        }
    }
}

$campaigns = index_campaigns();

?>


    <?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>

    <!--    [ Strat Section Title Area]-->
    <section id="welcome" class="">
        <div class="overlay">
            <div class="wel-come-areas owl-carousel">
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Connect. <span>Donate.</span> Impact.</h2>
                                    <h5>Your time to make a difference.</h5>
                                    <div class="welcome-btn">
                                        <a href="registration/home.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg-02 d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Verify. <span>Donate.</span> Validate.</h2>
                                    <h5>How many lives you can change today ?</h5>
                                    <div class="welcome-btn">
                                        <a href="registration/home.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->


<!--    [ Strat Section Area]-->
<section id="blog-type-project">
    <div class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>Campaigns</h2>
                    </div>
                </div>
            </div>
            <div class="row">

                <?php foreach($campaigns as $campaign): ?>
                <div class="col-lg-4">
                    <a href="details.php?campaign=<?php echo $campaign['campaign_id'] ?>" class="project-detail-content">
                        <div class="single-project-image">
                            <img src="<?php echo "site-photos/".$campaign['campaign_photo']; ?>" alt="">
                        </div>
                    </a>
                        <div class="project-detail-txt">
                            <div class="project-detail-funding">
                                <span><img src="<?php echo "site-photos/".organization_info_by_id($campaign['org_id'])['org_logo'] ?>" width="20px" height="20px"></span>
                                <span><?php echo organization_info_by_id($campaign['org_id'])['org_name'] ?></span>
                            </div>
                            <div class="project-heading-txt">
                                <a href="details.php?campaign=<?php echo $campaign['campaign_id'] ?>"><h4><?php echo $campaign['campaign_title'] ?></h4></a>
                            </div>
                            <div class="project-progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100?>%"></div>
                                    <p><?php echo round((((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100,2)?>%</p>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="pro-target-money">
                                            <h5>Target : <?php echo $campaign['target_money'] ?> BDT</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="pro-target-money text-right">
                                            <h5>Collected : <?php echo (double)campaign_money_earned($campaign['campaign_id']) ?> TK.</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="contribut">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="conti">
                                                <div class="pro-target-money">
                                                    <h5>Status: <?php echo $campaign['campaign_status'] ?></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 text-right">
                                            <div class="conti">
                                                <div class="pro-target-money">

                                                    <?php

                                                    if(isset($_SESSION['auth']) && $_SESSION['type']=='org')
                                                    {
                                                        $Url = 'dashboard/org/edit.php?campaign='.$campaign['campaign_id'];
                                                        $btn = "Edit";
                                                    }
                                                    else
                                                    {
                                                        $Url = 'donation/index.php?campaign='.$campaign['campaign_id'];
                                                        $btn = "Donate Now";
                                                    }
                                                    ?>

                                                    <a href="<?php echo $Url ?>"><?php echo $btn ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 <?php endforeach; ?>
                 </div>

            <div class="see-more">
                <?php if(!empty($campaigns)): ?>
                <a href="campaigns.php">More Campaigns</a>
                <?php else: ?>
                No campaign created.
                <?php endif; ?>
            </div>

        </div>
        </div>

    </section>
    <!--    [Finish Section Title Area]-->
    <!--    [ Strat Section Area]-->
    <section id="subscribe">
        <div class="section-padding">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <form action="index.php" method="post" enctype="multipart/form-data">
                            <div class="subscribtion-frm">
                                <div class="input-group mb-3">
                                    
                                    <input type="email" name="subscribe_email" class="form-control" placeholder="Subscribe to get latest campaigns">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2"><button type="submit" name="subscribe">Subscribe</button></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Area]-->

    <!--    [ Strat Section Area]-->
    <section id="mission">
        <div class="section-padding">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="mission-menu">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Behind The Scene</a>
                                </li>
                                <!--<li class="nav-item">-->
                                <!--    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">History</a>-->
                                <!--</li>-->
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>About</h4>
                            </div>
                            <div class="mission-txt">
                                <p>Donate.com.bd is a secure platform that gives innovators, artists and changemakers the opportunity to raise funds from their friends, family and our awesome donate.com.bd community for a fundraising project they want to create. Bangladesh is a land of potential where leaders/entrepreneurs/innovators can make an impact. Unfortunately, the changemakers are not empowered with proper funding so that they can make an impact. In donate.com.bd, the changemakers can post a fundraising video, market the project and collect funds from their well-wishers. 
</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>Behind the Scene</h4>
                            </div>
                            <div class="mission-txt">
                                <p>Donate.com.bd, known as Donate for Bangladesh is powered by Preneur Lab Limited. Preneur Lab is an award winning soical good company. The company honored with World Summit Award, Unilever Digital Week Award, BRAC Manthan Award and many more. The company is also featured in BBC, TEDx and many other national and international news outlets.</p>
                            <p>Donate.com.bd is supported and funded by ICT Division of Bangladesh Govt.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'footer.php'; ?>
