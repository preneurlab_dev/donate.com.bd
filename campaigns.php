<?php

require_once 'app/fun/functions.php';
require_once 'app/autoload.php';

$campaigns = '';

if(!empty($_GET['category']) && $_GET['category'] != 'all')
{
    $campaigns = campaigns_with_category($_GET['category']);
}
elseif ((!empty($_GET['category']) && $_GET['category'] == 'all') || empty($_GET['category']))
{
    $campaigns = campaigns();
}

?>


<?php set_page_title('Campaigns'); require_once 'header.php'; ?>


<!--    [ Strat Section Area]-->
<section id="blog-type-project">
    <div class="section-padding">
        <div class="container">

            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>All Campaigns</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <div class="row">

                        <?php if(!empty($campaigns)): ?>

                                <?php foreach($campaigns as $campaign): ?>
                            <div class="col-lg-4">
                                <a href="detail.php?campaign=<?php echo $campaign['campaign_id'] ?>" class="project-detail-content">
                                    <div class="single-project-image">
                                        <img src="<?php echo "site-photos/".$campaign['campaign_photo']; ?>">
                                    </div>
                                </a>
                                <div class="project-detail-txt org-campaign">
                                    <div class="project-detail-funding">
                                        <span><img src="<?php echo 'site-photos/'.organization_info_by_id($campaign['org_id'])['org_logo']; ?>" width="20px" height="20px"></span>
                                        <span><?php echo organization_info_by_id($campaign['org_id'])['org_name'] ?></span>
                                    </div>
                                    <div class="project-heading-txt">
                                        <h4><?php echo $campaign['campaign_title'] ?></h4>
                                    </div>
                                    <div class="project-progress">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100?>%"></div>
                                            <p><?php echo round((((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100,2)?>%</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="pro-target-money">
                                                    <h5>Target : <?php echo $campaign['target_money'] ?> BDT</h5>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="pro-target-money text-right">
                                                    <h5>Collected : <?php echo (double)campaign_money_earned($campaign['campaign_id']) ?> TK.</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="contribut">
                                            <div class="row">
                                                <div class="col-lg-5 padding">
                                                    <div class="conti margin-l-15">
                                                        <div class="pro-target-money">
                                                            <h5>Status: <?php echo $campaign['campaign_status'] ?></h5>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-7 text-right">

                                                    <div class="conti">
                                                        <div class="pro-target-money">
                                                            <a href="donation/index.php?campaign=<?php echo ($campaign['campaign_id']) ?>">Donate Now</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <?php endforeach; ?>
                        <?php else:?>
                        <?php echo 'No campaign enlisted.' ?>
                        <?php endif; ?>
                    </div>
                    
                </div>
                <div class="col-lg-2">
                    <div class="side-bar">
                        <div class="side-title text-right">
                            <h5>Category</h5>
                        </div>
                        <div class="side-ul text-right">
                            <ul>

                                <li><a href="campaigns.php?category=all">Show All</a></li>

                                <?php if(!empty(campaigns_category())): ?>

                                    <?php foreach (campaigns_category() as $category): ?>
                                        <li><a class="<?php ($_GET['category']==$category['category'] ? print 'active-color' : null) ?>" href="campaigns.php?category=<?php echo $category['category'] ?>"><?php echo ucfirst($category['category']) ?></a></li>
                                    <?php endforeach; ?>

                                    <?php else: ?>

                                    <li><a href="#">Empty Category</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                 </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <?php require_once 'footer.php'; ?>
