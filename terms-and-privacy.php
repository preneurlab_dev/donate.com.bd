<?php

require_once 'app/fun/functions.php';

?>

<?php set_page_title('Terms & Condition and Privacy Policy'); require_once 'header.php'; ?>

<div class="container">
    <div class="row justify-content-center section-padding-2">
        <div class="col-lg-8">
            <h3>Terms & Condition and Privacy Policy</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p>Last Modified Date 14 July 2018</p>
            <p>The Terms effective when you register in [donate.com.bd]. Terms and Conditions may change as our policy. <strong>Please read Terms and Conditions before donation. You accept Terms and Conditions by clicking check or submitting donation button</strong></p>

            <div>
                <h3>1. Payment</h3>

                <h4>Transaction</h4>
                <p>Please ensure you have enough amount that you want to donate. If any illegal way detect your account may be locked.</p>

                <h4>Transaction Fee</h4>
                <p>In every transaction no more than (3.00%) will be deduct to donate in [donate.com.bd]. Additional fee deduction depends on your transaction method.</p>

                <h4>Taxes</h4>
                <p>To donate in [donate.com.bd] no tax free is applicable for payment.</p>
            </div>

            <hr>

            <div>
                <h3>2. Data Privacy</h3>

                <h4>Payment Information</h4>
                <p>We do not store your payment card or account number. Now donation payment through <a href="http://www.shurjopay.com.bd/">shurjoPay</a> a trusted payment service that provides us your payment informations.</p>

                <h4>Personal Information</h4>
                <p>Your personal information such as. name, email, phone, address, photo we do not use these information for illegal purpose rather we use these information to ensure your identity.</p>

                <h4>Cookies</h4>
                <p>Cookie is a small amount of data send from user browser to website. Cookie used in our website to make user friendly and for better performance. We do not use store your cookie information.</p>

                <h4>Data Protection</h4>
                <p>We give value to protect your information from illegal and unauthorized use. We have a secure Authentication system that protects your valuable information.</p>
            </div>


        </div>
    </div>
</div>

<?php require('footer.php'); ?>
