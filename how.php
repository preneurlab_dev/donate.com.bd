<?php

require_once 'app/fun/functions.php';

?>

    <?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
    <style>
        .welcomte-content {
            width: 100%;
            height: 350px;
        }
    </style>

    <!--    [ Strat Section Title Area]-->
    <section id="welcome" class="">
        <div class="overlay">
            <div class="wel-come-areas owl-carousel">
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Connect. <span>Donate.</span> Impact.</h2>
                                    <h5>Your time to make a difference.</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="welcomte-content d-table">
                    <div class="welcome-txt welcome-bg-02 d-table-cell">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8 text-center">
                                    <h2>Verify. <span>Donate.</span> Validate.</h2>
                                    <h5>How many lives you can change today ?</h5>
                                    <div class="welcome-btn">
                                        <a href="http://donate.com.bd/demo/registration/index.php">REGISTER</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Title Area]-->



    <!--    [ Strat Section Area]-->
    <section id="mission">
        <div class="section-padding">
            <div class="container">
                

                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        
                        <div class="mission-content">
                            <div class="mission-title text-center">
                                <h4>How It Works ?</h4>
                                <img src="assets/img/donate_process.png"/>
                            </div>
                            <div class="mission-txt">
                                
                                <p>Any organization or individual can raise funds, collect goods or recruit volunteers in Donate.com.bd platform. The service is totally FREE. Organizations will be asked to submit offcial registrations documents via email once they register. This is to verify the registration. Once verified, organizations or individuals can create campaigns to receive donations from donors.</p>
                                <p>Donors can chose the organization or campaign they want to donate. Donations can be money or physical goods. bKash and credit cards can be used to donate money. the payment system is handled by ShurjoPay which is a Bangladesh Bank approved entity to do so. Donors and Organizations can check notifications in every steps. Every 2 weeks payments are transferred from payment gateway provider to organizatiosn bank accounts. Goods are collected by verified volunteers. Volunteers receive area-wise notifications to collect physical donations from pickup location added by the donor. When volunteer receives a donation, donors and organization receive notifications so they can verify the volunteer. Same happens when volunteer distribute the donation to the organizations.</p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>

    <?php require_once 'footer.php'; ?>
