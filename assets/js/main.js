(function ($) {
    "use strict";

    jQuery(document).ready(function () {

        /*  HOME Carousel
        ----------------------------------*/
        $(".wel-come-areas").owlCarousel({
            items: 1,
            dots: true,
            autoplay: true,
            nav: false,
            loop: true,

        });
        if ($.fn.on) {
            $(".wel-come-areas").on("translate.owl.carousel", function () {
                $(".welcome-txt h2").removeClass("animated fadeInUp").css("opacity", "0")
            });
            $(".wel-come-areas").on("translated.owl.carousel", function () {
                $(".welcome-txt h2").addClass("animated fadeInUp").css("opacity", "1")
            });
        }


        /*  - Scroll-top 
      	---------------------------------------------*/
        jQuery(window).on('scroll', function () {
            var scrollTop = jQuery(this).scrollTop();
            if (scrollTop > 400) {
                jQuery('.top').fadeIn();
            } else {
                jQuery('.top').fadeOut();
            }
        });

        jQuery('.top').on('click', function () {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);

            return false;
        });









    });


    jQuery(window).on("load", function () {
        /*  - Pre Loader
        ---------------------------------------------*/
        $(".pre-loader-area").fadeOut();

    });


}(jQuery));
