<?php

require_once 'app/fun/functions.php';

?>

<?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>


<section id="payment-status">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5">

                    <?php if(isset($_GET['status']) && $_GET['status'] == 'success'): ?>
                    <div class="currect-sign">
                        <div class="card">
                            <div class="roght-sign text-center">
                                <i class="icofont icofont-check-alt"></i>
                            </div>
                            <div class="roght-sign-content text-center">
                                <h5>Great ! <span>Your Payment is Successful.</span></h5>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(isset($_GET['status']) && $_GET['status'] == 'failed'): ?>
                    <div class="currect-sign">
                        <div class="card">
                            <div class="roght-sign wrong-bg text-center">
                                <i class="icofont icofont-close-line"></i>
                            </div>
                            <div class="roght-sign-content text-center">
                                <h5>Opps ! <span><?php (isset($_GET['msg'])) ? print $_GET['msg'] : print 'Payment Not Success' ?></span></h5>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>

<?php require('footer.php'); ?>
