<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$organization_info = organization_info();

if(isset($_POST['org_info_update']))
{
    if(org_info_update() == true)
    {
        header('Location: profile.php?msg=updated');
    }

}

if(isset($_POST['change_logo']))
{
    if(!empty($_FILES['org_logo']['size']))
    {
        if(logo_update() == true)
        {
            header('Location: profile.php?msg=updated');
        }

    }
    else
    {
        header('Location: profile.php?msg=nothing');
    }
}

if(isset($_POST['update_password']))
{
    if($_POST['new_password'] == $_POST['confirm_password'])
    {
        if(verify_old_password('org') == true)
        {
            if(change_password('org') == true)
            {
                header('Location: profile.php?msg=updated');
            }

        }
        else
        {
            header('Location: profile.php?msg=wrong_pass');
        }
    }
    else
    {
        header('Location: profile.php?msg=not_matched');
    }


}

?>

    <?php set_page_title(organization_name()); require_once 'header.php'; ?>
    <section id="edit-pro">
        <div class="section-padding">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <?php if(isset($_GET['msg']) && $_GET['msg']=='updated'): ?>
                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <strong>Success:</strong> Your Profile Updated Successfully.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($_GET['msg']) && $_GET['msg']=='wrong_pass'): ?>
                            <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                                <strong>Danger:</strong> Your Old Password is Wrong.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($_GET['msg']) && $_GET['msg']=='not_matched'): ?>
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> New Password and Confirm Password Not Matched.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($_GET['msg']) && $_GET['msg']=='nothing'): ?>
                            <div class="alert alert-info alert-dismissible fade show text-center" role="alert">
                                <strong>Info:</strong> Nothing to Update.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-3">
                        <div class="nav flex-column nav-pills pro-side-btn" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Organization Information</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Organization Logo</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Change Password</a>
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="pro-form">
                                    <div class="row">
                                        <div class="col-md-8 m-md-auto text-center">
                                            <div class="section-title bg-dark-title">
                                                <h4>Organization Information</h4>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Start Personal Information -->
                                    <form action="profile.php" method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-form">

                                                </div>
                                                <div class="single-form">

                                                </div>
                                                <div class="single-form">

                                                </div>
                                            </div>
                                            <div class="col-lg-8">

                                                <input type="hidden" name="org_id" value="<?php echo $organization_info['org_id'] ?>">

                                                <div class="single-form">
                                                    <label for="volunteer_name">Name</label>
                                                    <input id="org_name" name="org_name" type="text" required  placeholder="Enter Your Name" value="<?php echo $organization_info['org_name'] ?>">
                                                </div>
                                                <div class="single-form">
                                                    <label for="volunteer_phone">Reg. No.</label>
                                                    <input id="org_reg_no" name="org_reg_no" type="text" required placeholder="Enter Registration Number" value="<?php echo $organization_info['org_reg_no'] ?>">
                                                </div>
                                                <div class="single-form">
                                                    <label for="volunteer_address">Address</label>
                                                    <textarea id="org_address" name="org_address" required placeholder="Enter Your Address"><?php echo $organization_info['org_address'] ?></textarea>
                                                </div>
                                                <div class="single-btn">
                                                    <button type="submit" name="org_info_update">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- End Personal Information -->

                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="pro-pic">
                                    <div class="row">
                                        <div class="col-md-8 m-md-auto text-center">
                                            <div class="section-title bg-dark-title">
                                                <h4>Company Logo</h4>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Start Change Photo -->
                                    <form action="profile.php" method="post" enctype="multipart/form-data">

                                        <input type="hidden" name="org_id" value="<?php echo $organization_info['org_id'] ?>">

                                        <div class="row justify-content-end">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="user-pic pro-pic-up-btn text-center">
                                                            <img id="test" src="<?php echo "../../site-photos/".$organization_info['org_logo'] ?>">
                                                            <button><i class="icofont icofont-image"></i></button>
                                                            <input id="tstpic" name="org_logo" type="file">
                                                            <input id="tstpic" type="hidden" name="old_photo" value="<?php echo $organization_info['org_logo']?>">

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="user-pic-up">
                                                            <div class="single-btn">
                                                                <button type="submit" name="change_logo">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- End Change Photo -->

                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <div class="col-md-8 m-md-auto text-center">
                                        <div class="section-title bg-dark-title">
                                            <h4>Change Password</h4>
                                        </div>
                                    </div>
                                </div>

                                <!-- Start Change Password -->
                                <div class="pro-form">
                                    <form action="profile.php" method="post" enctype="multipart/form-data">

                                        <input type="hidden" name="org_id" value="<?php echo $organization_info['org_id'] ?>">


                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="single-form">

                                                </div>
                                                <div class="single-form">

                                                </div>
                                                <div class="single-form">

                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="single-form">
                                                    <label for="name">Old Password</label>
                                                    <input id="old_password" name="old_password" type="password" required placeholder="Old Password">
                                                </div>
                                                <div class="single-form">
                                                    <label for="phone">New Password</label>
                                                    <input id="new_password" name="new_password" type="password" required placeholder="New Password">
                                                </div>
                                                <div class="single-form">
                                                    <label for="address">Confirm Password</label>
                                                    <input id="confirm_password" name="confirm_password" type="password" required placeholder="Confirm New Password">
                                                </div>
                                                <div class="single-btn">
                                                    <button type="submit" name="update_password">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- End Change Password -->
                            </div>
                            <div class="tab-pane fade" id="v-pills-login" role="tabpanel" aria-labelledby="v-pills-settings-tab">05...</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php require('footer.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#test').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#tstpic").change(function() {
                readURL(this);
            }

        );

    </script>
