<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$goods_donations_submissions = donor_goods_donation_submissions();
$volunteer_accepted_goods_list_without_confirm = volunteer_accepted_goods_list_without_confirm();
$volunteer_accepted_goods_list_with_confirm = volunteer_accepted_goods_list_with_confirm();

if(isset($_GET['confirm_goods']))
{
    if(volunteer_goods_donation_confirm_process() == true)
    {
        header('Location: goods-history.php');
    }
}


?>

<?php set_page_title('Goods History'); require_once 'header.php'; ?>
<section id="search">
    <div class="section-padding">
        <div class="container">
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Goods Donations</h3>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mission-menu">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="goods-requests-tab" data-toggle="tab" href="#goods-submission" role="tab" aria-controls="goods-submission" aria-selected="true">Submissions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="volunteer-accepted-tab" data-toggle="tab" href="#volunteer-accepted" role="tab" aria-controls="volunteer-accepted" aria-selected="false">Volunteer Accepted</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="confirmed-list-tab" data-toggle="tab" href="#confirmed-list" role="tab" aria-controls="confirmed-list" aria-selected="false">Confirmed List</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>

                    <!-- Requests-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="goods-submission" role="tabpanel" aria-labelledby="goods-submission-tab">
                            <!--<div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($goods_donations_submissions as $submissions): ?>
                                                <tr>
                                                    <td><?php echo $submissions['goods_serial'] ?></td>
                                                    <td><?php echo ($submissions['campaign_id'] != 'None') ? campaign_title_by_id($submissions['campaign_id']): $submissions['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($submissions['donor_id']) ?></td>
                                                    <td><?php echo $submissions['category'] ?></td>
                                                    <td><?php echo $submissions['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$submissions['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',strtotime($submissions['issue_date'])) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Volunteer Accepted-->
                        <div class="tab-pane fade" id="volunteer-accepted" role="tabpanel" aria-labelledby="volunteer-accepted-tab">
                       <!--     <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Volunteer</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Action</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($volunteer_accepted_goods_list_without_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id'] != 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($list['donor_id']) ?></td>
                                                    <td><?php echo volunteer_name_by_id($list['volunteer']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td>
                                                        <a href="goods-history.php?confirm_goods=<?php echo $list['goods_serial'] ?>" onclick="return confirm('Are you really received Goods?');">Confirm</a>
                                                    </td>
                                                    <td><?php echo date('d M Y',strtotime($list['issue_date'])) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Confirmed List-->
                        <div class="tab-pane fade" id="confirmed-list" role="tabpanel" aria-labelledby="confirmed-list-tab">
                        <!--    <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Volunteer</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($volunteer_accepted_goods_list_with_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id'] != 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($list['donor_id']) ?></td>
                                                    <td><?php echo volunteer_name_by_id($list['volunteer']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',strtotime($list['issue_date'])) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once 'footer.php'; ?>
