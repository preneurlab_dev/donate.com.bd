<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

if(empty($_GET['campaign']))
{
    header('Location: campaigns.php');
}

$campaign = campaign_info_by_id($_GET['campaign']);

if(isset($_POST['campaign_edit']))
{
    if(campaign_edit() == true)
    {
        header('Location: detail.php?msg=updated&campaign='.$_POST['campaign_id']);
    }
}

?>

    <?php set_page_title('Campaign Edit | donate.com.bd'); require_once 'header.php'; ?>
    <section id="edit-pro">
        <div class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="nav flex-column nav-pills pro-side-btn" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Campaign Title</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Campaign Description</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Campaign Image</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Campaign Time</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-login" role="tab" aria-controls="v-pills-settings" aria-selected="false">Campaign Target</a>
                        </div>
                    </div>
                    <div class="col-9">
                        <form action="edit.php" class="needs-validation" novalidate  method="post" enctype="multipart/form-data">

                            <input type="hidden" name="campaign_id" value="<?php echo $_GET['campaign']?>">


                           <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="pro-form">
                                    <div class="row">
                                        <div class="col-md-4 m-md-auto">
                                            <div class="section-title bg-dark-title">
                                                <h4>Update Campaign Title</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">

                                        </div>
                                        <div class="col-lg-8">
                                            <div class="single-form">
                                                <label for="name">Campaign Title</label>
                                            </div>
                                            <div class="single-form">
                                                <input id="campaign_title" class="form-control" name="campaign_title" type="text" placeholder="Enter Campaign Title" value="<?php echo $campaign['campaign_title']?>" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign title.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="pro-pic">
                                    <div class="row">
                                        <div class="col-md-4 m-md-auto">

                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="single-form">
                                                        <label for="category">Campaign Category</label>
                                                        <select name="category" id="category" required>

                                                            <?php if(isset($campaign['category'])): ?>
                                                            <option value="<?php echo $campaign['category']?>"><?php echo ucfirst($campaign['category'])?></option>
                                                            <?php endif; ?>

                                                            <option value="">Select Category</option>
                                                            <option value="food">Food</option>
                                                            <option value="women">Women</option>
                                                            <option value="children">Children</option>
                                                        </select>
                                                        <div class="invalid-feedback">
                                                            Please enter campaign category.
                                                        </div>
                                                    </div>
                                                    <div class="single-form">
                                                        <label for="name">Campaign Description</label>
                                                    </div>
                                                    <div class="single-form">
                                                        <textarea id="campaign_desc" name="campaign_desc" id="campaign_desc" placeholder="Enter Campaign Description" required><?php echo $campaign['campaign_desc']?></textarea>
                                                        <div class="invalid-feedback">
                                                            Please enter campaign details.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="user-pic-up">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

                                <div class="row justify-content-end">
                                    <div class="col-lg-8">
                                        <div class="section-title bg-dark-title">
                                            <h4>Update Campaign Photo</h4>
                                        </div>
                                        <div class="subsc-form">
                                            <div class="user-pic pro-pic-up-btn campaing-single-pic text-center">
                                                <img id="test" src="<?php echo "../../site-photos/".$campaign['campaign_photo']?>" alt="">
                                                <button><i class="icofont icofont-image"></i></button>
                                                <input id="tstpic" name="campaign_photo" type="file" required>
                                                <input id="tstpic" type="hidden" name="old_photo" value="<?php echo $campaign['campaign_photo']?>">
                                                <div class="invalid-feedback">
                                                    Please upload campaign photo.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <div class="col-md-8 m-md-auto text-center">

                                    </div>
                                </div>
                                <div class="pro-form">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="section-title bg-dark-title">
                                                <h3>Update Campaign Time</h3>
                                            </div>
                                            <div class="single-form">
                                                <label for="name">Campaign Start Time</label>
                                            </div>
                                            <div class="single-form">
                                                <input name="campaign_start" class="form-control" id="name" type="date" value="<?php echo $campaign['campaign_start']?>" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign start time.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="phone">Campaign End Time</label>
                                            </div>
                                            <div class="single-form">
                                                <input name="campaign_end" class="form-control" id="phone" type="date" value="<?php echo $campaign['campaign_end']?>" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign end time.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
 
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-login" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <div class="col-md-6 m-md-auto">

                                    </div>
                                </div>
                                <div class="row justify-content-end">

                                    <div class="col-lg-8">
                                        <div class="section-title bg-dark-title">
                                            <h3>Update Campaign Target Amount</h3>
                                        </div>
                                        <div class="single-form">
                                            <label for="name">Campaign Target Amount</label>
                                        </div>
                                        <div class="single-form">
                                            <input id="target_amount" class="form-control" name="target_amount" type="text" placeholder="Campaign Target Amount" value="<?php echo $campaign['target_money']?>" required>
                                            <div class="invalid-feedback">
                                                Please enter campaign target amount.
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-lg-3">
                                <div class="canpaign-sub-btn">
                                    <div class="single-btn text-right">
                                        <button type="submit" name="campaign_edit">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php require('footer.php'); ?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#test').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#tstpic").change(function() {
                readURL(this);
            }

        );

    </script>
