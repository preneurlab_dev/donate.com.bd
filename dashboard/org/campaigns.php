<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$campaigns = null;

if(!empty($_GET['category']) && $_GET['category'] != 'all')
{
    $campaigns = org_campaigns_with_category($_GET['category']);
}
elseif ((!empty($_GET['category']) && $_GET['category'] == 'all') || empty($_GET['category']))
{
    $campaigns = org_campaigns_all();
}

if(isset($_GET['action']))
{
    if($_GET['action'] === "delete")
    {
        if(campaign_delete() == true)
        {
            header('Location: campaigns.php?msg=deleted');
        }
    }
}

if(isset($_GET['action']))
{
    if($_GET['action'] === 'completed')
    {
        if(campaign_completed() == true)
        {
            header('Location: campaigns.php?msg=completed');
        }
    }
}

?>


<?php set_page_title('Campaigns'); require_once 'header.php'; ?>



<!--    [ Strat Section Area]-->
<section id="blog-type-project">
    <div class="section-padding">
        <div class="container">

            <?php if(isset($_GET['msg']) && $_GET['msg'] == 'deleted'): ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                        <strong>Success:</strong> Campaign Deleted Successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-md-8 m-md-auto text-center">
                    <div class="section-title bg-dark-title">
                        <h2>All Campaigns</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <div class="row">
                            <?php foreach($campaigns as $campaign): ?>
                        <div class="col-lg-4">
                            <a href="detail.php?campaign=<?php echo $campaign['campaign_id'] ?>" class="project-detail-content">
                                <div class="single-project-image">
                                    <img src="<?php echo "../../site-photos/".$campaign['campaign_photo']; ?>">
                                </div>
                            </a>
                            <div class="project-detail-txt org-campaign">
                                <div class="project-detail-funding">
                                    <span><img src="<?php echo '../../site-photos/'.organization_info()['org_logo']; ?>" width="20px" height="20px"></span>
                                    <span><?php echo organization_info()['org_name'] ?></span>
                                </div>
                                <div class="project-heading-txt">
                                    <h4><a href="detail.php?campaign=<?php echo $campaign['campaign_id'] ?>"><?php echo $campaign['campaign_title'] ?></a></h4>
                                </div>
                                <div class="project-progress">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo (((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100?>%"></div>
                                        <p><?php echo round((((double)campaign_money_earned($campaign['campaign_id']))/((double)$campaign['target_money']))*100,2)?>%</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="pro-target-money">
                                                <h5>Target : <?php echo $campaign['target_money'] ?> BDT</h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="pro-target-money text-right">
                                                <h5>Collected : <?php echo (double)campaign_money_earned($campaign['campaign_id']) ?> TK.</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="contribut">
                                        <div class="conti">
                                            <div class="pro-target-money">
                                                <h5>Status: <?php echo $campaign['campaign_status'] ?></h5>
                                            </div>
                                        </div>
                                        <div class="conti">
                                            <div class="pro-target-money">
                                                <a href="campaigns.php?action=delete&campaign_id=<?php echo ($campaign['campaign_id']) ?>" onclick="return confirm('Are you sure you want to delete this item?');">Delete <i class="icofont icofont-delete-alt"></i></a>
                                                <a href="edit.php?campaign=<?php echo ($campaign['campaign_id']) ?>">Edit <i class="icofont icofont-ui-edit"></i></a>

                                                <?php if($campaign['campaign_status'] !== 'End'): ?>
                                                    <a href="campaigns.php?action=completed&campaign=<?php echo ($campaign['campaign_id']) ?>"">Complete <i class="icofont icofont-dart"></i></a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5 padding">

                                            </div>

                                            <div class="col-lg-7 text-right">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     <?php endforeach; ?>
                    </div>
                    
                </div>
                <div class="col-lg-2">
                    <div class="side-bar">
                        <div class="side-title text-right">
                            <h5>Category</h5>
                        </div>
                        <div class="side-ul text-right">
                            <ul>

                                <?php if(!empty(org_campaign_category())): ?>

                                    <?php foreach (org_campaign_category() as $category): ?>
                                        <li><a class="<?php ($_GET['category']==$category['category'] ? print 'active-color' : null) ?>" href="campaigns.php?category=<?php echo $category['category'] ?>"><?php echo ucfirst($category['category']) ?></a></li>
                                    <?php endforeach; ?>

                                    <?php else: ?>

                                    <li><a href="#">Empty Category</a></li>

                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
                
                 </div>
            </div>
        </div>
    </section>
    <!--    [Finish Section Title Area]-->


    <?php require_once 'footer.php'; ?>
