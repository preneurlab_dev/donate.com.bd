<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

if(isset($_POST['add_campaign']))
{
    $reg_status = add_campaign();

    if($reg_status === 'invalid_file_type')
    {
        header('Location: add.php?msg=invalid_file_type');
    }
    if($reg_status === 'empty_fields')
    {
        header('Location: add.php?msg=empty_fields');
    }
    elseif($reg_status === true)
    {
        header('Location: add.php?msg=added');
    }
}

?>

    <?php set_page_title('Add Campaign | donate.com.bd'); require_once 'header.php'; ?>

    <section id="edit-pro">
        <div class="section-padding">
            <div class="container">


                <div class="row">
                    <div class="col-lg-12">
                        <?php if(isset($_GET['msg']) && $_GET['msg']=='added'): ?>
                            <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                <strong>Success:</strong> You have added campaign successfully.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($_GET['msg']) && $_GET['msg']=='invalid_file_type'): ?>
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> Only allowed <strong>jpeg, png, gif</strong> type image.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($_GET['msg']) && $_GET['msg']=='empty_fields'): ?>
                            <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Warning:</strong> All fields are required.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="nav flex-column nav-pills pro-side-btn" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Campaign Title</a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Campaign Details</a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Campaign Image</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Campaign Time</a>
                            <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-login" role="tab" aria-controls="v-pills-settings" aria-selected="false">Campaign Target</a>
                        </div>
                    </div>
                    <div class="col-9">
                        <form action="add.php" class="needs-validation" novalidate method="post" enctype="multipart/form-data">

                           <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                <div class="pro-form">
                                    <div class="row justify-content-end">
                                        <div class="col-md-4 m-md-auto">
                                            <div class="section-title bg-dark-title">
                                                <h4>Campaign Title</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"> </div>
                                        <div class="col-lg-8">
                                            <div class="single-form">
                                                <label for="name">Campaign Title</label>
                                            </div>
                                            <div class="single-form">
                                                <input id="campaign_title" name="campaign_title" type="text" class="form-control" placeholder="Enter Campaign Title" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign title.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div class="pro-pic">
                                    <div class="row">
                                        <div class="col-md-4 m-md-auto">
                                            <div class="section-title bg-dark-title">
                                                <h4>Campaign Details</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row justify-content-end">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="single-form">
                                                        <label for="category">Campaign Category</label>
                                                        <select name="category" class="form-control" id="category" required>
                                                            <option value="Select Category">Select Category</option>
                                                            <option value="food">Food</option>
                                                            <option value="women">Women</option>
                                                            <option value="children">Children</option>
                                                        </select>
                                                        <div class="invalid-feedback">
                                                            Please enter campaign category.
                                                        </div>
                                                    </div>
                                                    <div class="single-form">
                                                        <label for="name">Campaign Details</label>
                                                        <textarea id="campaign_desc" name="campaign_desc" class="form-control" id="campaign_desc" placeholder="Enter Campaign Description" required></textarea>
                                                        <div class="invalid-feedback">
                                                            Please enter campaign details.
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="user-pic-up">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                <div class="row">
                                    <div class="col-md-4 m-md-auto">
                                        <div class="section-title bg-dark-title">
                                            <h4>Campaign Image</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-lg-8">
                                        <div class="subsc-form">
                                            <div class="user-pic pro-pic-up-btn campaing-single-pic text-center">
                                                <img id="test" src="../../assets/img/upload.jpg" alt="">
                                                <button><i class="icofont icofont-image"></i></button>
                                                <input id="tstpic" name="campaign_photo" class="form-control" type="file" required>
                                                <div class="invalid-feedback">
                                                    Please upload campaign photo.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <div class="col-md-4 m-md-auto">
                                        <div class="section-title bg-dark-title">
                                            <h3>Campaign Time</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="pro-form">
                                    <div class="row">
                                        <div class="col-lg-4">


                                        </div>
                                        <div class="col-lg-8">
                                            <div class="single-form">
                                                <label for="name">Campaign Start Time</label>
                                            </div>
                                            <div class="single-form">
                                                <input name="campaign_start" class="form-control" id="name" type="date" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign start time.
                                                </div>
                                            </div>
                                            <div class="single-form">
                                                <label for="phone">Campaign End Time</label>
                                            </div>
                                            <div class="single-form">
                                                <input name="campaign_end" class="form-control" id="phone" type="date" required>
                                                <div class="invalid-feedback">
                                                    Please enter campaign end time.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
 
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-login" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                <div class="row">
                                    <div class="col-md-4 m-md-auto">
                                        <div class="section-title bg-dark-title">
                                            <h4>Target Amount</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">

                                    </div>
                                    <div class="col-lg-8">
                                        <div class="single-form">
                                            <label for="name">Campaign Target Amount</label>
                                        </div>
                                        <div class="single-form">
                                            <input id="target_amount" name="target_amount" class="form-control" type="text" placeholder="Campaign Target Amount" required>
                                            <div class="invalid-feedback">
                                                Please enter campaign target amount.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-lg-3">
                                        <div class="canpaign-sub-btn">
                                            <div class="single-btn text-right">
                                                <button type="submit" name="add_campaign">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div> 
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php require('footer.php');?>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#test').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#tstpic").change(function() {
                readURL(this);
            }

        );

    </script>


    <?php get_alert_message() ?>
