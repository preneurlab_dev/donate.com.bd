<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$transactions_logs = donor_money_transaction_logs();

?>

<?php set_page_title('Money Donations'); require_once 'header.php'; ?>
<section id="search">
    <div class="section-padding">
        <div class="container">
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Money Donation Transactions</h3>
                    </div>

                    <div class="mission-content">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Campaign</th>
                                        <th>Organization</th>
                                        <th>Payment Option</th>
                                        <th>Bank Status</th>
                                        <th>SP Code Status</th>
                                        <th>Bank Trx. ID</th>
                                        <th>Amount (TK.)</th>
                                        <th>Issue Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($transactions_logs as $logs): ?>
                                        <tr>
                                            <td><?php echo $logs['serial'] ?></td>
                                            <td><?php echo ($logs['campaign_id']!='None') ? campaign_title_by_id($logs['campaign_id']) : $logs['campaign_id']?></td>
                                            <td><?php echo organization_name_by_id($logs['org_id']) ?></td>
                                            <td><?php echo $logs['sp_payment_option'] ?></td>
                                            <td><?php echo $logs['bank_status'] ?></td>
                                            <td><?php echo $logs['sp_code_des'] ?></td>
                                            <td><?php echo $logs['bank_tx_id'] ?></td>
                                            <td><?php echo (double)$logs['amount'] ?></td>
                                            <td><?php echo date('d M Y',strtotime($logs['issue_date'])) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once 'footer.php'; ?>
