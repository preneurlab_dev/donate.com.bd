<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$goods_donations_list_without_confirm = donor_goods_donations_list_without_confirm();
$goods_donations_list_with_confirm = donor_goods_donations_list_with_confirm();


?>

<?php set_page_title('Goods Donations'); require_once 'header.php'; ?>
<section id="search">
    <div class="section-padding">
        <div class="container">
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Goods Donations</h3>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mission-menu">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="goods-requests-tab" data-toggle="tab" href="#goods-submission" role="tab" aria-controls="goods-submission" aria-selected="true">Submissions</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="confirmed-list-tab" data-toggle="tab" href="#confirmed-list" role="tab" aria-controls="confirmed-list" aria-selected="false">Confirmed List</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>

                    <!-- Requests-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="goods-submission" role="tabpanel" aria-labelledby="goods-submission-tab">
                            <!--<div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Organization</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($goods_donations_list_without_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id']!= 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo organization_name_by_id($list['org_id']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',$list['issue_date']) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Confirmed List-->
                        <div class="tab-pane fade" id="confirmed-list" role="tabpanel" aria-labelledby="confirmed-list-tab">
                           <!-- <div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered"
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Organization</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($goods_donations_list_with_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id']!= 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo organization_name_by_id($list['org_id']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',$list['issue_date']) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once 'footer.php'; ?>
