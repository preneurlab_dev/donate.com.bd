<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';

unauthorized_user_redirect('../../login.php');

$total_volunteering = total_volunteering();


?>

<?php set_page_title('Dashboard'); require_once 'header.php'; ?>
<section id="history">
    <div class="section-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="mission-menu text-center">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#dn-money" role="tab" aria-controls="home" aria-selected="true">Money Donation</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="awards-tab" data-toggle="tab" href="#awards" role="tab" aria-controls="awards" aria-selected="false">Awards</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="money-donation-history">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="dn-money" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row justify-content-center">
                            <div class="col-lg-6">
                                <div class="all-donate-info">
                                    <hr>
                                    <h5>Total Volunteering <span><?php echo (int)$total_volunteering ?></span></h5>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="awards" role="tabpanel" aria-labelledby="awards-tab">
                        <div class="mission-content">
                            <div class="card">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Award Name</th>
                                            <th scope="col">Award</th>
                                            <th scope="col">Wining Fact</th>
                                            <th scope="col">Wining Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Mark</td>
                                            <td>Otto</td>
                                            <td>@mdo</td>
                                            <td>@mdo</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">3</th>
                                            <td colspan="2">Larry the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section id="search">
    <div class="section-padding">
        <div class="container">

        </div>
    </div>
</section>
<?php require('footer.php'); ?>
