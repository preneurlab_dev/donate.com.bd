<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title><?php get_page_title() ?></title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="../../assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="../../assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="../../assets/css/style.css">
    <link rel="stylesheet" href="../../assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="../../assets/img/favicon.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->
    <!--<div class="pre-loader-area">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>-->


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat Logo Area]-->
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
                    <a class="navbar-brand" href="../../index.php">
                        <div class="logo">
                            <img src="../../assets/img/logo.png" alt="">
                        </div>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../../index.php">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="volunteering.php">Volunteering</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                    <div class="head-user">
                                        <span class="usr-nam-bld"><?php echo volunteer_name() ?></span>
                                        <img src="<?php echo '../../site-photos/'.volunteer_info()['volunteer_photo'] ?>"/>
                                    </div>

                                </a>
                                <div class="dropdown-menu user-down margin-lr" aria-labelledby="navbarDropdown">
                                    <div class="row padding">
                                        <div class="col-lg-3">
                                            <div class="head-d-user">
                                                <img src="<?php echo '../../site-photos/'.volunteer_info()['volunteer_photo'] ?>"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 padding">
                                            <div class="head-user-info">
                                                <h6><?php echo volunteer_name() ?></h6>
                                                <p><?php echo user_info()['user_email'] ?></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item single-droupdown-item padding" href="profile.php">Profile</a>
                                    <a class="dropdown-item single-droupdown-item padding" href="#">Help</a>
                                    <a class="dropdown-item single-droupdown-item padding" href="logout.php">Log Out</a>
                                </div>
                            </li>


                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!--    [Finish Logo Area]-->
    </header>
    <!--    [Finish Header Area]-->
