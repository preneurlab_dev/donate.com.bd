<?php

require_once '../../app/fun/functions.php';
require_once '../../app/autoload.php';


unauthorized_user_redirect('../../login.php');


$goods_collection_requests_list = volunteer_goods_collection_requests_list();
$own_accepted_list_without_confirm = volunteer_own_accepted_list_without_confirm();
$own_accepted_list_with_confirm = volunteer_own_accepted_list_with_confirm();
$accept_collect_goods_success = false;

if (isset($_GET['accept']))
{
    if(volunteer_goods_collection_accept_process() == true)
    {
        header('Location: volunteering.php');
    }
}

?>

<?php set_page_title('donate.com.bd'); require_once 'header.php'; ?>
<section id="search">
    <div class="section-padding">
        <div class="container">
            <div class="donate-money-list">
                <div class="card">
                    <div class="donate-money-list-title text-center">
                        <h3>Goods Collection Requests</h3>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <?php if ($accept_collect_goods_success == true): ?>
                                <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                    <strong>Thank you for volunteering. We will notify you after found desired goods.</strong>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="mission-menu">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="goods-requests-tab" data-toggle="tab" href="#goods-requests" role="tab" aria-controls="goods-requests" aria-selected="true">Requests</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="my-accepts-tab" data-toggle="tab" href="#my-accepts" role="tab" aria-controls="volunteer-accepted" aria-selected="false">My Accepts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="confirmed-list-tab" data-toggle="tab" href="#confirmed-list" role="tab" aria-controls="confirmed-list" aria-selected="false">Confirmed List</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>

                    <!-- Requests -->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="goods-requests" role="tabpanel" aria-labelledby="goods-requests-tab">
                            <!--<div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->

                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($goods_collection_requests_list as $request): ?>
                                                <tr>
                                                    <td><?php echo $request['goods_serial'] ?></td>
                                                    <td><?php echo ($request['campaign_id'] != 'None') ? campaign_title_by_id($request['campaign_id']): $request['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($request['donor_id']) ?></td>
                                                    <td><?php echo $request['category'] ?></td>
                                                    <td><?php echo $request['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$request['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',strtotime($request['issue_date'])) ?></td>
                                                    <td>
                                                        <a href="volunteering.php?accept=<?php echo $request['goods_serial'] ?>">Accept</a>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- My Accepts-->
                        <div class="tab-pane fade" id="my-accepts" role="tabpanel" aria-labelledby="my-accepts-tab">
                            <!--<div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($own_accepted_list_without_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id'] != 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($list['donor_id']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',strtotime($list['issue_date'])) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Confirmed List-->
                        <div class="tab-pane fade" id="confirmed-list" role="tabpanel" aria-labelledby="confirmed-list-tab">
                            <!--<div class="row justify-content-end">
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p>Filter</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="singleofild">
                                                <select>
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <div class="singleofild">
                                                <button><i class="icofont icofont-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="mission-content">
                                <div class="card">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Campaign</th>
                                                <th>Donor</th>
                                                <th>Category</th>
                                                <th>Location</th>
                                                <th>Photo</th>
                                                <th>Issue Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($own_accepted_list_with_confirm as $list): ?>
                                                <tr>
                                                    <td><?php echo $list['goods_serial'] ?></td>
                                                    <td><?php echo ($list['campaign_id'] != 'None') ? campaign_title_by_id($list['campaign_id']): $list['campaign_id'] ?></td>
                                                    <td><?php echo donor_name_by_id($list['donor_id']) ?></td>
                                                    <td><?php echo $list['category'] ?></td>
                                                    <td><?php echo $list['location'] ?></td>
                                                    <td><img src="<?php echo '../../site-photos/'.$list['photo'] ?>" width="100px" height="50px"></td>
                                                    <td><?php echo date('d M Y',strtotime($list['issue_date'])) ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require_once 'footer.php'; ?>
